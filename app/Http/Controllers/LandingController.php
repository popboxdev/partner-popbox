<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LandingController extends Controller
{

    public function index()
    {

        $data['title'] = 'PopSend v2';
        return view('layouts.main', $data);

    }

    public function contact()
    {

        $data['title'] = 'PopSend v2';
        return view('pages.contact', $data);

    }

    public function apps()
    {

        $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
        $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
        $iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
        $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
        $webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

        //do something with this information
        if( $iPod || $iPhone ){
            //browser reported as an iPhone/iPod touch -- do something here
            $string = "Location: https://itunes.apple.com/us/app/popbox-asia/id1196265583?ls=1";
            header($string);
            die();
        }else if($iPad){
            //browser reported as an iPad -- do something here
            $string = "Location: https://itunes.apple.com/us/app/popbox-asia/id1196265583?ls=1";
            header($string);
            die();
        }else if($Android){
            //browser reported as an Android device -- do something here
            $string = "Location: https://play.google.com/store/apps/details?id=asia.popbox.app";
            header($string);
            die();
        }else if($webOS){
            //browser reported as a webOS device -- do something here
            $string = "Location: https://itunes.apple.com/us/app/popbox-asia/id1196265583?ls=1";
            header($string);
            die();
        }else{
            //browser reported as PC -- do something here
            $string = "Location: https://popsend.popbox.asia/";
            header($string);
            die();
        }


    }





}
