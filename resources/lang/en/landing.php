<?php

return [
	'home' => 'Home',
	'step' => 'Steps to Use',
	'contact' => 'Contact',
	'close' => 'CLOSE',
	'navigation' => 'Navigation',
	'backtotop' => 'Click to Return on The Top Page',

	'about' => 'PopBox is an automated parcel locker that allows you to select your preferred locker and try out the experience of using PopBox.',
	'download' => 'Download NOW!',
	'or' => 'or',

	'services' => 'Our Services',
	'popsend_about' => 'New way to send your parcel from one PopBox locker to another or to the selected address',
	'popsafe_about' => 'Reliable service which provides convenience to store your belongings in a PopBox locker safely',
	'ondemand_about' => 'New experience of cleaning your laundry via PopBox services with ease',
	'freegift_about' => 'Get an attractive gift for your participation in the survey',

	'popsend_step' => 'Steps to Use PopSend',
	'popsend_step_one_title' => 'Place the parcel in the locker',
	'popsend_step_one_desc' => 'Select a pick-up location and a drop-off location. Confirm your order and attach the order receipt to the parcel before placing the parcel in the locker. The courier will collect the parcel and send to your drop-off location.',
	'popsend_step_two_title' => 'Multi-locations for destinations',
	'popsend_step_two_desc' => 'Within 1 (one) transaction, you can directly add on up to 5 (five) destinations for lockers.',
	'popsend_step_three_title' => 'Ensure that the information is complete',
	'popsend_step_three_desc' => 'Fill in the necessary details to ensure that your parcel reaches the destination quickly.',
	'popsend_step_four_title' => 'Attach the invoice and send',
	'popsend_step_four_desc' => 'The invoice will be sent to your email after the transaction. Print the invoice and attach it to the parcel which is ready to be sent.',

	'popsafe_step' => 'Steps to Use PopSafe',
	'popsafe_step_one_title' => 'Select locker location',
	'popsafe_step_one_desc' => 'You can rent a locker at your preferred location.',
	'popsafe_step_two_title' => 'Select locker size',
	'popsafe_step_two_desc' => 'Select the locker size which is suitable to store your belongings.',
	'popsafe_step_three_title' => 'Store your belongings',
	'popsafe_step_three_desc' => 'Open the locker by keying in the order number.',
	'popsafe_step_four_title' => 'Collect within 24 hours',
	'popsafe_step_four_desc' => 'The PIN Code will be sent to you once the order is made which is valid for 24 hours to store your belongings.',

	'ondemand_step' => 'Steps to Use On Demand',
	'ondemand_step_one_title' => 'Select our partner',
	'ondemand_step_one_desc' => 'Select PopBox’s partner and the service that you need, for example, the laundry service.',
	'ondemand_step_two_title' => 'Store items in the locker',
	'ondemand_step_two_desc' => 'Place your laundry items in the locker with your order number.',
	'ondemand_step_three_title' => 'Working process and payment',
	'ondemand_step_three_desc' => 'Your laundry will be processed by PopBox’s partner. Please remember to make the payment.',
	'ondemand_step_four_title' => 'Collect items',
	'ondemand_step_four_desc' => 'You will receive a PIN Code to retrieve your items when the process is completed.',

	'freegift_step' => 'Steps to Use Free Gift',
	'freegift_step_one_title' => 'Fill in the online survey form',
	'freegift_step_one_desc' => 'Spare some time to answer our survey questions.',
	'freegift_step_two_title' => 'Fill in your personal details',
	'freegift_step_two_desc' => 'Provide your complete personal details.',
	'freegift_step_three_title' => 'Receive a surprise from PopBox',
	'freegift_step_three_desc' => 'You will receive a surprise from PopBox if you are lucky. Wait for the SMS with PIN Code to collect the gift.',

	'phone' => '+6001110606011',
	'email' => 'info@popbox.asia',
	'address' => '',

];