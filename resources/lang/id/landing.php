<?php

return [
	'home' => 'Beranda',
	'step' => 'Cara Menggunakan',
	'contact' => 'Kontak',
	'close' => 'TUTUP',
	'navigation' => 'Menu Navigasi',
	'backtotop' => 'Tekan untuk Kembali ke Atas',

	'about' => 'PopBox smart locker untuk kebutuhan sehari-hari kamu. Pilih lokasi loker favorit kamu dan coba pengalaman menggunakan PopBox Loker.',
	'download' => 'Unduh Sekarang',
	'or' => 'atau',

	'services' => 'Layanan Kami',
	'popsend_about' => 'Cara baru untuk mengirim paket dari loker dengan tujuan ke loker PopBox atau ke alamat',
	'popsafe_about' => 'Layanan yang memberikan kemudahan untuk menyimpan barang bawaan di loker PopBox dengan aman',
	'ondemand_about' => 'Pengalaman baru membersihkan pakaian dan sepatu kotor serta memperbaiki tas dan sebagainya yang rusak menggunakan loker PopBox',
	'freegift_about' => 'Dapatkan bingkisan menarik untuk setiap partisipasi kamu',

	'popsend_step' => 'Cara Menggunakan PopSend',
	'popsend_step_one_title' => 'Letakkan Di Loker Atau Jemput Paket',
	'popsend_step_one_desc' => 'Pilih lokasi asal paket, kamu dapat meletakkan sendiri di loker terdekat atau kurir kami mengambil di alamat kamu.',
	'popsend_step_two_title' => 'Multi Lokasi Untuk Tujuan',
	'popsend_step_two_desc' => 'Dalam 1 (satu) transaksi kamu dapat langsung menambahkan sampai 5 (lima) lokasi tujuan baik loker atau alamat.',
	'popsend_step_three_title' => 'Pastikan Data Lengkap',
	'popsend_step_three_desc' => 'Isi detail data yang dibutuhkan agar memastikan barang sampai ditujuan dengan cepat.',
	'popsend_step_four_title' => 'Tempel Struk dan Kirim',
	'popsend_step_four_desc' => 'Template label paket dikirimkan ke email kamu setelah transaksi, cetak dan tempelkan di paket, dan paket siap dikirim.',

	'popsafe_step' => 'Cara Menggunakan PopSafe',
	'popsafe_step_one_title' => 'Pilih Lokasi Loker',
	'popsafe_step_one_desc' => 'Kamu bisa menyewa satu kompartemen di loker favoritmu.',
	'popsafe_step_two_title' => 'Pilih Loker',
	'popsafe_step_two_desc' => 'Pilih ukuran loker yang sesuai dengan barang yang ingin kamu simpan.',
	'popsafe_step_three_title' => 'Simpan Barang',
	'popsafe_step_three_desc' => 'Datang ke loker dan buka loker menggunakan nomor order pesanan kamu.',
	'popsafe_step_four_title' => '1 x 24 Jam',
	'popsafe_step_four_desc' => 'Kamu akan dikirimkan PIN Code untuk mengambil barang yang valid sampai 1 x 24 jam setelah order dibuat.',

	'ondemand_step' => 'Cara Menggunakan On Demand',
	'ondemand_step_one_title' => 'Pilih Partner Kami',
	'ondemand_step_one_desc' => 'Pilih partner PopBox dan layanan yang kamu inginkan, pilihannya terdiri dari laundri, perbaikan, dan perawatan sepatu.',
	'ondemand_step_two_title' => 'Simpan Barang Di Loker',
	'ondemand_step_two_desc' => 'Letakkan baju/sepatu kamu ke loker dengan nomor order kamu.',
	'ondemand_step_three_title' => 'Proses Pengerjaan & Pembayaran',
	'ondemand_step_three_desc' => 'Baju/sepatu kamu akan diproses oleh partner PopBox dan jangan lupa lakukan pembayaran.',
	'ondemand_step_four_title' => 'Pengambilan Barang',
	'ondemand_step_four_desc' => 'Kamu akan dikirimkan PIN Code untuk mengambil barang jika prosesnya sudah selesai.',

	'freegift_step' => 'Cara Menggunakan Free Gift',
	'freegift_step_one_title' => 'Isi Survey',
	'freegift_step_one_desc' => 'Luangkan waktumu untuk mengisi beberapa pertanyaan kami.',
	'freegift_step_two_title' => 'Masukkan data diri',
	'freegift_step_two_desc' => 'Isi data diri kamu lengkap.',
	'freegift_step_three_title' => 'Kejutan dari PopBox',
	'freegift_step_three_desc' => 'Jika beruntung kamu akan mendapatkan hadiah dari PopBox, tunggu SMS PIN Code utuk mengambil hadiahnya.',
	
	'phone' => '+622122538719',
	'email' => 'info@popbox.asia',
	'address' => '<li><img src="https://popsend.popbox.asia/img/address.png" class="img-responsive"></li><li class="address-resize">Jl. Palmerah Utara III no 62 FGH Palmerah<br>Jakarta Barat 11480 - Indonesia</li>',
];