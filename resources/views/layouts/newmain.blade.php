<!DOCTYPE html>
<html lang="en">
<head>
    <title>PopBox - Smart Locker Indonesia</title>
    <meta name="description" content="PopBox smart locker for your daily needs. Choose our service for feel PopBox Locker Experience.">
    <meta name="author" content="Popbox Asia">
    <meta name="keywords" content="parcel locker, electronic locker, loker otomatis, loker pintar, smart locker, kirim barang, logistik, delivery, fast delivery, parcel delivery, logistic, parcel, parcel locker indonesia, pengiriman cepat, kurir">
    <meta property="og:url" content="popsend.popbox.asia" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="PopBox - Smart Locker Indonesia" />
    <meta property="og:keywords" content="parcel locker, electronic locker, loker otomatis, loker pintar, smart locker, kirim barang, logistik, delivery, fast delivery, parcel delivery, logistic, parcel, parcel locker indonesia, pengiriman cepat, kurir" />
    <meta property="og:description" content="PopBox smart locker for your daily needs. Choose our service for feel PopBox Locker Experience." />
    <meta property="og:image" content="{{ asset('/img/metaimage.png') }}" />

    <!--favicon-->
    <link rel="shortcut icon" href="{{ asset('/img/favico128.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('/img/favico128.ico') }}" type="image/x-icon">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{ asset('libs/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/nivo-lightbox/nivo-lightbox.css') }}">
    <link rel="stylesheet" href="{{ asset('css/nivo-lightbox/nivo-lightbox-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/colors/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('flag-icon-css-master/css/flag-icon.min.css') }}">

    <!-- Google Fonts -->
    <link href='{{ asset('fonts/css6ef7.css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic|Roboto+Condensed:300italic,400italic,700italic,400,300,700') }}' rel='stylesheet' type='text/css'>
    <link href='{{ asset('fonts/css838e.css?family=Open+Sans:400,300,700') }}' rel='stylesheet' type='text/css'>

    <!-- Modernizr to provide HTML5 support for IE. -->
    <script src="{{ asset('js/modernizr.custom.js') }}"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-72128831-5', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body id="home">

<!-- ****************************** Preloader ************************** -->
<div id="preloader"></div>


<!-- ****************************** Sidebar ************************** -->
<nav id="sidebar-wrapper">
    <a id="menu-close" href="#" class="close-btn toggle">@lang('landing.close')  <i class="ion-log-in"></i></a>
    <ul class="sidebar-nav">
        <li><a href="#home">@lang('landing.home')</a></li>
        <li><a href="#bigfeatures">@lang('landing.services')</a></li>
        <li><a href="#features-image">@lang('landing.step')</a></li>
        <li><a href="#footer">@lang('landing.contact')</a></li>
    </ul>
</nav>


<!-- ****************************** Header ************************** -->
<header class="sticky" id="header">
    <div class="container">
        <div class="row" id="logo_menu">
            <div class="col-xs-6"><a class="logo" href="#"><img src="img/logo-dark.png" alt=""></a></div>
            <div class="col-xs-5" style=" margin-top: 12px;">
            <div class="change-lang" style="z-index: 1; float: right; color: #fff; margin-top: 20px 0px; padding: 0px 10px; line-height: 0;">
                    
                    <div class="text-center"><a id="setToID" href="javascript:void(0)" class="flag-icon flag-icon-id toggle" style="margin-right: 20px; margin-top: -8px;" data-toggle="tooltip" data-placement="bottom" title="Bahasa"></a>
                    <a id="setToEN" href="javascript:void(0)" class="flag-icon flag-icon-gb" data-toggle="tooltip" data-placement="bottom" title="English"></a></div>
                </div>
            </div>
            <div class="col-xs-1">
                
                <a id="menu-toggle" href="#" class="toggle" role="button" title="@lang('landing.navigation')" data-toggle="tooltip" data-placement="left"><i class="ion-navicon"></i></a>
            </div>
        </div>
    </div>
</header>



<!-- ****************************** Banner ************************** -->
<section id="banner" >
    <div class="banner-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 hidden-xs hidden-sm">
                <div class="hand-container">
                    <img class="iphone-hand img_res wow animated bounceInUp" data-wow-duration="1.2s" src="{{ url('/img/header-apps.png') }}" alt="">
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="headings">
                    <h2 class="wow animated fadeInDown">
                        <strong>@lang('landing.about')</strong>
                    </h2>
                    <p class="wow animated fadeInLeft"></p>

                    <p class="wow animated fadeInRight pad-top-10">@lang('landing.download')</p>

                    <div class="row">
                        <div class="col-md-4">
                            <center><img class="img-responsive" src="{{ url('img/qr.png') }}" alt=""></center><br>
                        </div>
                        <div class="col-md-2">
                            <ul class="display-block">
                                <li><div class="header-vertical-border"></div></li>
                                <li class="header-text">@lang('landing.or')</li>
                                <li><div class="header-vertical-border"></div></li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <a href="https://play.google.com/store/apps/details?id=asia.popbox.app" onclick="downloadApps(1)" target="_blank" class="btn btn-store wow animated bounceInUp">
                                <img class="img-responsive" src="{{ url('/img/playstore.png') }}">
                            </a>
                            <a href="https://itunes.apple.com/us/app/popbox-asia/id1196265583?ls=1&mt=8" onclick="downloadApps(2)" target="_blank" class="btn btn-store wow animated bounceInUp">
                                <img class="img-responsive" src="{{ url('/img/appstore.png') }}">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- ****************************** Big Features section ************************** -->
<section id="bigfeatures" class="img-block-3col block">

    <div class="container">

        <div class="title-area text-center">
            <h1 class="section-title">@lang('landing.services')</h1>
            <!--
            <hr class="title-under-line-top" />
            <hr class="title-under-line-bottom" />
            -->
        </div>

        <div class="row">
            <div class="col-sm-12 hidden-lg hidden-md text-center">
                <div class="animation-box wow bounceIn animated">
                    <img class="highlight-left wow animated" src="img/spark.png" height="192" width="48" alt="">
                    <img class="highlight-right wow animated" src="img/spark.png" height="192" width="48" alt="">
                    <img class="screen" src="{{ url('/img/menu-app.png') }}" alt="" height="581" width="300">
                </div>
            </div>
        </div>

        <div class="row margin-top-50">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12 height-200">
                        <div class="row">
                            <div class="col-md-2 hidden-md hidden-lg">
                                <div class="bullet-popsend">
                                    <div class="icon-test">
                                        <span class="x"> &nbsp; </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h2 class="title-popsend text-right">PopSend</h2>
                            </div>
                            <div class="col-md-2 hidden-sm hidden-xs">
                                <div class="bullet-popsend">
                                    <div class="icon-test">
                                        <span class="x"> &nbsp; </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 text-right">
                            @lang('landing.popsend_about')
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 height-200">
                        <div class="row">
                            <div class="col-md-2 hidden-md hidden-lg">
                                <div class="bullet-popsafe">
                                    <div class="icon-test">
                                        <span class="x"> &nbsp; </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h2 class="title-popsafe text-right">PopSafe</h2>
                            </div>
                            <div class="col-md-2 hidden-sm hidden-xs">
                                <div class="bullet-popsafe">
                                    <div class="icon-test">
                                        <span class="x"> &nbsp; </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 text-right">
                            @lang('landing.popsafe_about')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center hidden-sm hidden-xs">
                <div class="animation-box wow bounceIn animated">
                    <img class="highlight-left wow animated" src="img/spark.png" height="192" width="48" alt="">
                    <img class="highlight-right wow animated" src="img/spark.png" height="192" width="48" alt="">
                    <img class="screen" src="{{ url('/img/menu-app.png') }}" alt="" height="581" width="300">
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12 height-200">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="bullet-ondemand">
                                    <div class="icon-test">
                                        <span class="x"> &nbsp; </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h2 class="title-ondemand">On Demand</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-offset-2 col-md-8">
                            @lang('landing.ondemand_about')
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 height-200">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="bullet-freegift">
                                    <div class="icon-test">
                                        <span class="x"> &nbsp; </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h2 class="title-freegift">Free Gift</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-offset-2 col-md-8">
                            @lang('landing.freegift_about')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="features-image" class="block" style="background: #f7f7f7;">
    <div class="container" style="background: #f7f7f7;">

        <div class="row">
            <div class="col-sm-6 wow fadeInLeft animated">
                <div class="phone-image">
                    <img class="img-responsive" src="{{ url('/img/popsend.png') }}" alt="" id="product-popsend-img">
                    <img class="img-responsive hide" src="{{ url('/img/popsafe.png') }}" alt="" id="product-popsafe-img">
                    <img class="img-responsive hide" src="{{ url('/img/pod.png') }}" alt="" id="product-ondemand-img">
                    <img class="img-responsive hide" src="{{ url('/img/freegift.png') }}" alt="" id="product-freegift-img">
                </div>
            </div>
            <div class="col-sm-6 wow fadeInRight animated">

                <div class="title-area">
                    <h1 class="section-title" id="product-title-popsend" style="margin-left: 0px; padding-left: 0px;">@lang('landing.step') PopSend</h1>
                    <h1 class="section-title hide" id="product-title-popsafe">@lang('landing.step') PopSafe</h1>
                    <h1 class="section-title hide" id="product-title-ondemand">@lang('landing.step') On Demand</h1>
                    <h1 class="section-title hide" id="product-title-freegift">@lang('landing.step') Free Gift</h1>
                </div>

                <div class="row">
                    <div class="col-md-12 causes-content" id="product-popsend-content">
                        <div class="row">
                            <div class="col-md-1 col-xs-2"><span class="bullet-detail active">1</span></div>
                            <div class="col-md-11 col-xs-10"><strong>@lang('landing.popsend_step_one_title')</strong></div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><img src="{{ url('/img/arrow.png') }}" class="arrow"></div>
                            <div class="col-md-9 col-xs-10">@lang('landing.popsend_step_one_desc')</div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><span class="bullet-detail">2</span></div>
                            <div class="col-md-11 col-xs-10"><strong>@lang('landing.popsend_step_two_title')</strong></div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><img src="{{ url('/img/arrow.png') }}" class="arrow"></div>
                            <div class="col-md-9 col-xs-10">@lang('landing.popsend_step_two_desc')</div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><span class="bullet-detail">3</span></div>
                            <div class="col-md-11 col-xs-10"><strong>@lang('landing.popsend_step_three_title')</strong></div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><img src="{{ url('/img/arrow.png') }}" class="arrow"></div>
                            <div class="col-md-9 col-xs-10">@lang('landing.popsend_step_three_desc')</div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><span class="bullet-detail">4</span></div>
                            <div class="col-md-11 col-xs-10"><strong>@lang('landing.popsend_step_four_title')</strong></div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2">&nbsp;</div>
                            <div class="col-md-9 col-xs-10">@lang('landing.popsend_step_four_desc')</div>
                        </div>
                    </div>
                    <div class="col-md-12 causes-content hide" id="product-popsafe-content">
                        <div class="row">
                            <div class="col-md-1 col-xs-2"><span class="bullet-detail-popsafe active">1</span></div>
                            <div class="col-md-11 col-xs-10"><strong>@lang('landing.popsafe_step_one_title')</strong></div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><img src="{{ url('/img/arrow.png') }}" class="arrow"></div>
                            <div class="col-md-9 col-xs-10">@lang('landing.popsafe_step_one_desc')</div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><span class="bullet-detail-popsafe">2</span></div>
                            <div class="col-md-11 col-xs-10"><strong>@lang('landing.popsafe_step_one_title')</strong></div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><img src="{{ url('/img/arrow.png') }}" class="arrow"></div>
                            <div class="col-md-9 col-xs-10">@lang('landing.popsafe_step_two_desc')</div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><span class="bullet-detail-popsafe">3</span></div>
                            <div class="col-md-11 col-xs-10"><strong>@lang('landing.popsafe_step_three_title')</strong></div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><img src="{{ url('/img/arrow.png') }}" class="arrow"></div>
                            <div class="col-md-9 col-xs-10">@lang('landing.popsafe_step_three_desc')</div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><span class="bullet-detail-popsafe">4</span></div>
                            <div class="col-md-11 col-xs-10"><strong>@lang('landing.popsafe_step_four_title')</strong></div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2">&nbsp;</div>
                            <div class="col-md-9 col-xs-10">@lang('landing.popsafe_step_four_desc')</div>
                        </div>
                    </div>
                    <div class="col-md-12 causes-content hide" id="product-pod-content">
                        <div class="row">
                            <div class="col-md-1 col-xs-2"><span class="bullet-detail-pod active">1</span></div>
                            <div class="col-md-11 col-xs-10"><strong>@lang('landing.ondemand_step_one_title')</strong></div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><img src="{{ url('/img/arrow.png') }}" class="arrow"></div>
                            <div class="col-md-9 col-xs-10">@lang('landing.ondemand_step_one_desc')</div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><span class="bullet-detail-pod">2</span></div>
                            <div class="col-md-11 col-xs-10"><strong>@lang('landing.ondemand_step_two_title')</strong></div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><img src="{{ url('/img/arrow.png') }}" class="arrow"></div>
                            <div class="col-md-9 col-xs-10">@lang('landing.ondemand_step_two_desc')</div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><span class="bullet-detail-pod">3</span></div>
                            <div class="col-md-11 col-xs-10"><strong>@lang('landing.ondemand_step_three_title')</strong></div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><img src="{{ url('/img/arrow.png') }}" class="arrow"></div>
                            <div class="col-md-9 col-xs-10">@lang('landing.ondemand_step_three_desc')</div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><span class="bullet-detail-pod">4</span></div>
                            <div class="col-md-11 col-xs-10"><strong>@lang('landing.ondemand_step_four_title')</strong></div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2">&nbsp;</div>
                            <div class="col-md-9 col-xs-10">@lang('landing.ondemand_step_four_desc')</div>
                        </div>
                    </div>
                    <div class="col-md-12 causes-content hide" id="product-freegift-content">
                        <div class="row">
                            <div class="col-md-1 col-xs-2"><span class="bullet-detail-freegift active">1</span></div>
                            <div class="col-md-11 col-xs-10"><strong>@lang('landing.freegift_step_one_title')</strong></div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><img src="{{ url('/img/arrow.png') }}" class="arrow"></div>
                            <div class="col-md-9 col-xs-10">@lang('landing.freegift_step_one_desc')</div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><span class="bullet-detail-freegift">2</span></div>
                            <div class="col-md-11 col-xs-10"><strong>@lang('landing.freegift_step_two_title')</strong></div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><img src="{{ url('/img/arrow.png') }}" class="arrow"></div>
                            <div class="col-md-9 col-xs-10">@lang('landing.freegift_step_two_desc')</div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2"><span class="bullet-detail-freegift">3</span></div>
                            <div class="col-md-11 col-xs-10"><strong>@lang('landing.freegift_step_three_title')</strong></div>
                        </div>
                        <div class="row mar-top-15">
                            <div class="col-md-1 col-xs-2">&nbsp;</div>
                            <div class="col-md-9 col-xs-10">@lang('landing.freegift_step_three_desc')</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 wow fadeInDown animated">
                <div class="row product">
                    <div class="col-md-3 col-sm-6 col-xs-6 small-product">
                        <center><a href="javascript:void(0)" onclick="changeProduct(1)" id="menu-popsend" class="display-block footer-popsend"><img src="{{ url('/img/btn-popsend.png') }}" class="img-responsive" style="width: 45%; height: auto;"><br>PopSend</a></center>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 small-product">
                        <center><a href="javascript:void(0)" onclick="changeProduct(2)" id="menu-popsafe" class="display-block opacity5 footer-popsafe"><img src="{{ url('/img/btn-popsafe.png') }}" class="img-responsive" style="width: 50%; height: auto;"><br>PopSafe</a></center>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 small-product">
                        <center><a href="javascript:void(0)" onclick="changeProduct(3)" id="menu-pod" class="display-block opacity5 footer-pod"><img src="{{ url('/img/btn-pod.png') }}" class="img-responsive" style="width: 50%; height: auto;"><br>On Demand</a></center>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 small-product">
                        <center><a href="javascript:void(0)" onclick="changeProduct(4)" id="menu-freegift" class="display-block opacity5 footer-freegift"><img src="{{ url('/img/btn-freegift.png') }}" class="img-responsive" style="width: 50%; height: auto;"><br>Free Gift</a></center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@include('elements.footer')


<!-- All the scripts -->
<script src="{{ asset('libs/jquery/1.11.1/jquery.min.js') }}"></script>
<script src="{{ asset('libs/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/wow.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.js') }}"></script>
<script src="{{ asset('js/nivo-lightbox.min.js') }}"></script>
<script src="{{ asset('js/smoothscroll.js') }}"></script>
<script src="{{ asset('js/jquery.ajaxchimp.min.js') }}"></script>
<script src="{{ asset('js/script.js') }}"></script>
<script src="{{ asset('js/contact.js') }}"></script>
<script src="{{ asset('js/jquery.easy-ticker.js') }}"></script>
<script src="{{ asset('js/jquery.magnific-popup.js') }}"></script>
<script>
    function changeProduct(product) {
        if(product == 1) {
            clickProduct(1);
            $('#product-popsend-img').removeClass("hide");
            $('#product-title-popsend').removeClass("hide");

            // opacity5
            $('#menu-popsend').removeClass("opacity5");
            $('#menu-popsafe').addClass("opacity5");
            $('#menu-pod').addClass("opacity5");
            $('#menu-freegift').addClass("opacity5");

            // hide
            $('#product-popsafe-img').addClass("hide");
            $('#product-title-popsafe').addClass("hide");
            $('#product-ondemand-img').addClass("hide");
            $('#product-title-ondemand').addClass("hide");
            $('#product-freegift-img').addClass("hide");
            $('#product-title-freegift').addClass("hide");

            // content
            $('#product-popsend-content').removeClass("hide");
            $('#product-popsafe-content').addClass("hide");
            $('#product-pod-content').addClass("hide");
            $('#product-freegift-content').addClass("hide");
        } else if(product == 2) {
            clickProduct(2);
            $('#product-popsafe-img').removeClass("hide");
            $('#product-title-popsafe').removeClass("hide");

            // opacity5
            $('#menu-popsend').addClass("opacity5");
            $('#menu-popsafe').removeClass("opacity5");
            $('#menu-pod').addClass("opacity5");
            $('#menu-freegift').addClass("opacity5");

            // hide
            $('#product-popsend-img').addClass("hide");
            $('#product-title-popsend').addClass("hide");
            $('#product-ondemand-img').addClass("hide");
            $('#product-title-ondemand').addClass("hide");
            $('#product-freegift-img').addClass("hide");
            $('#product-title-freegift').addClass("hide");

            // content
            $('#product-popsend-content').addClass("hide");
            $('#product-popsafe-content').removeClass("hide");
            $('#product-pod-content').addClass("hide");
            $('#product-freegift-content').addClass("hide");
        } else if(product == 3) {
            clickProduct(3);
            $('#product-ondemand-img').removeClass("hide");
            $('#product-title-ondemand').removeClass("hide");

            $('#menu-popsend').addClass("opacity5");
            $('#menu-popsafe').addClass("opacity5");
            $('#menu-pod').removeClass("opacity5");
            $('#menu-freegift').addClass("opacity5");

            // hide
            $('#product-popsend-img').addClass("hide");
            $('#product-title-popsend').addClass("hide");
            $('#product-popsafe-img').addClass("hide");
            $('#product-title-popsafe').addClass("hide");
            $('#product-freegift-img').addClass("hide");
            $('#product-title-freegift').addClass("hide");

            // content
            $('#product-popsend-content').addClass("hide");
            $('#product-popsafe-content').addClass("hide");
            $('#product-pod-content').removeClass("hide");
            $('#product-freegift-content').addClass("hide");
        } else if(product == 4) {
            clickProduct(4);
            $('#product-freegift-img').removeClass("hide");
            $('#product-title-freegift').removeClass("hide");

            $('#menu-popsend').addClass("opacity5");
            $('#menu-popsafe').addClass("opacity5");
            $('#menu-pod').addClass("opacity5");
            $('#menu-freegift').removeClass("opacity5");

            // hide
            $('#product-popsend-img').addClass("hide");
            $('#product-title-popsend').addClass("hide");
            $('#product-popsafe-img').addClass("hide");
            $('#product-title-popsafe').addClass("hide");
            $('#product-ondemand-img').addClass("hide");
            $('#product-title-ondemand').addClass("hide");

            // content
            $('#product-popsend-content').addClass("hide");
            $('#product-popsafe-content').addClass("hide");
            $('#product-pod-content').addClass("hide");
            $('#product-freegift-content').removeClass("hide");
        }
    }

    function clickProduct(id) {
        if(id == 1) {
            ga('send', {
                hitType: 'event',
                eventCategory: 'ProductUse',
                eventAction: 'click',
                eventLabel: 'PopSend'
            });
        } else if(id == 2) {
            ga('send', {
                hitType: 'event',
                eventCategory: 'ProductUse',
                eventAction: 'click',
                eventLabel: 'PopSafe'
            });
        } else if(id == 3) {
            ga('send', {
                hitType: 'event',
                eventCategory: 'ProductUse',
                eventAction: 'click',
                eventLabel: 'OnDemand'
            });
        } else if(id == 4) {
            ga('send', {
                hitType: 'event',
                eventCategory: 'ProductUse',
                eventAction: 'click',
                eventLabel: 'FreeGift'
            });
        }
    }

    function downloadApps(id) {
        if(id == 1) {
            ga('send', {
                hitType: 'event',
                eventCategory: 'Download Apps',
                eventAction: 'click',
                eventLabel: 'Google Play'
            });
        } else if(id == 2) {
            ga('send', {
                hitType: 'event',
                eventCategory: 'Download Apps',
                eventAction: 'click',
                eventLabel: 'App Store'
            });
        }
    }

    function clickSocials(id) {
        if(id == 1) {
            ga('send', {
                hitType: 'event',
                eventCategory: 'Social Media',
                eventAction: 'click',
                eventLabel: 'Facebook'
            });
        } else if(id == 2) {
            ga('send', {
                hitType: 'event',
                eventCategory: 'Social Media',
                eventAction: 'click',
                eventLabel: 'Instagram'
            });
        } else if(id == 3) {
            ga('send', {
                hitType: 'event',
                eventCategory: 'Social Media',
                eventAction: 'click',
                eventLabel: 'Youtube'
            });
        } else if(id == 4) {
            ga('send', {
                hitType: 'event',
                eventCategory: 'Social Media',
                eventAction: 'click',
                eventLabel: 'Twitter'
            });
        } else if(id == 5) {
            ga('send', {
                hitType: 'event',
                eventCategory: 'Social Media',
                eventAction: 'click',
                eventLabel: 'Line'
            });
        }
    }

    $("#setToID").click(function() {
        // alert( "Set to ID" );
        $.ajax({
            url: "{{ url('/id') }}",
            success: function(result){
                //
                window.location.href = "id";
            }
        });
    });

    $("#setToEN").click(function() {
        $.ajax({
            url: "{{ url('en') }}",
            success: function(result){
                //
                window.location.href = "en";
            }
        });
    });
</script>
</body>
</html>