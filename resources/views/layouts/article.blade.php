<!DOCTYPE html>
<html lang="en">
<head>
    <title>PopSend by PopBox</title>
    <meta name="description" content="Popbox Popsend">
    <meta name="author" content="Popbox Asia">
    <meta name="keywords" content="Popsend, Popbox, Locker">
    <meta property="og:url" content="popsend.popbox.asia" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="PopSend By PopBox" />
    <meta property="og:description" content="New way to send your parcel. Locate, select, drop at a PopBox near you and PopSend will deliver. Join PopSend today for the PopBox experience!" />
    <meta property="og:image" content="https://popsend.popbox.asia/img/meta-img.png" />

    <!--favicon-->
    <link rel="shortcut icon" href="{{ asset('css/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('css/favicon.ico') }}" type="image/x-icon">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{ asset('libs/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/nivo-lightbox/nivo-lightbox.css') }}">
    <link rel="stylesheet" href="{{ asset('css/nivo-lightbox/nivo-lightbox-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/colors/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

    <!-- Google Fonts -->
    <link href='{{ asset('fonts/css6ef7.css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic|Roboto+Condensed:300italic,400italic,700italic,400,300,700') }}' rel='stylesheet' type='text/css'>
    <link href='{{ asset('fonts/css838e.css?family=Open+Sans:400,300,700') }}' rel='stylesheet' type='text/css'>

    <!-- Modernizr to provide HTML5 support for IE. -->
    <script src="{{ asset('js/modernizr.custom.js') }}"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-72128831-5"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-72128831-5');
    </script>

</head>
<body id="home">

<!-- ****************************** Preloader ************************** -->
<div id="preloader"></div>


<!-- ****************************** Sidebar ************************** -->
<nav id="sidebar-wrapper">
    <a id="menu-close" href="#" class="close-btn toggle">CLOSE <i class="ion-log-in"></i></a>
    <ul class="sidebar-nav">
        <li><a href="#home">Home</a></li>
        <li><a href="#video">Video</a></li>
        <li><a href="#bigfeatures">Features</a></li>
        <li><a href="#features">Specialty</a></li>
        <li><a href="#features-left-image">With Image</a></li>
        <li><a href="#testimonial">Testimonial</a></li>
        <li><a href="#team">Team</a></li>
        <li><a href="#pricing">Pricing</a></li>
        <li><a href="#subscribe">Subscribe</a></li>
        <li><a href="#contact">Contact us</a></li>
    </ul>
</nav>


<!-- ****************************** Header ************************** -->
<header class="sticky background-red" id="header">
    <div class="container">
        <div class="row" id="logo_menu">
            <div class="col-xs-6"><a class="logo" href="#"><img src="img/logo-dark.png" alt=""></a></div>
            <div class="col-xs-6"><a id="menu-toggle" href="#" class="toggle" role="button" title="Navigation" data-toggle="tooltip" data-placement="left"><i class="ion-navicon"></i></a></div>
        </div>
    </div>
</header>

<section id="contact" class="block">
    <div class="container article-block">
        @yield('content')
    </div>
</section>

@include('elements.footer')

<!-- All the scripts -->
<script src="{{ asset('libs/jquery/1.11.1/jquery.min.js') }}"></script>
<script src="{{ asset('libs/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/wow.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.js') }}"></script>
<script src="{{ asset('js/nivo-lightbox.min.js') }}"></script>
<script src="{{ asset('js/smoothscroll.js') }}"></script>
<script src="{{ asset('js/jquery.ajaxchimp.min.js') }}"></script>
<script src="{{ asset('js/script.js') }}"></script>
<script src="{{ asset('js/contact.js') }}"></script>
<script src="{{ asset('js/jquery.easy-ticker.js') }}"></script>
<script src="{{ asset('js/jquery.magnific-popup.js') }}"></script>

</body>
</html>