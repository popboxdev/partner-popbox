<!DOCTYPE html>
<html lang="en">
<head>
    <title>PopSend by PopBox</title>
    <meta name="description" content="Popbox Popsend">
    <meta name="author" content="Popbox Asia">
    <meta name="keywords" content="Popsend, Popbox, Locker">
    <meta property="og:url" content="popsend.popbox.asia" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="PopSend By PopBox" />
    <meta property="og:description" content="New way to send your parcel. Locate, select, drop at a PopBox near you and PopSend will deliver. Join PopSend today for the PopBox experience!" />
    <meta property="og:image" content="https://popsend.popbox.asia/img/meta-img.png" />

    <!--favicon-->
    <link rel="shortcut icon" href="{{ asset('css/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('css/favicon.ico') }}" type="image/x-icon">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{ asset('libs/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/nivo-lightbox/nivo-lightbox.css') }}">
    <link rel="stylesheet" href="{{ asset('css/nivo-lightbox/nivo-lightbox-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/colors/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

    <!-- Google Fonts -->
    <link href='{{ asset('fonts/css6ef7.css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic|Roboto+Condensed:300italic,400italic,700italic,400,300,700') }}' rel='stylesheet' type='text/css'>
    <link href='{{ asset('fonts/css838e.css?family=Open+Sans:400,300,700') }}' rel='stylesheet' type='text/css'>

    <!-- Modernizr to provide HTML5 support for IE. -->
    <script src="{{ asset('js/modernizr.custom.js') }}"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-72128831-5', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body id="home">

<!-- ****************************** Preloader ************************** -->
<div id="preloader"></div>


<!-- ****************************** Sidebar ************************** -->
<nav id="sidebar-wrapper">
    <a id="menu-close" href="#" class="close-btn toggle">CLOSE <i class="ion-log-in"></i></a>
    <ul class="sidebar-nav">
        <li><a href="#home">Home</a></li>
        <li><a href="#video">Video</a></li>
        <li><a href="#bigfeatures">Features</a></li>
        <li><a href="#features">Specialty</a></li>
        <li><a href="#features-left-image">With Image</a></li>
        <li><a href="#testimonial">Testimonial</a></li>
        <li><a href="#team">Team</a></li>
        <li><a href="#pricing">Pricing</a></li>
        <li><a href="#subscribe">Subscribe</a></li>
        <li><a href="#contact">Contact us</a></li>
    </ul>
</nav>


<!-- ****************************** Header ************************** -->
<header class="sticky" id="header">
    <div class="container">
        <div class="row" id="logo_menu">
            <div class="col-xs-6"><a class="logo" href="#"><img src="img/logo-dark.png" alt=""></a></div>
            <div class="col-xs-6"><a id="menu-toggle" href="#" class="toggle" role="button" title="Navigation" data-toggle="tooltip" data-placement="left"><i class="ion-navicon"></i></a></div>
        </div>
    </div>
</header>



<!-- ****************************** Banner ************************** -->
<section id="banner" >
    <div class="banner-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 hidden-xs hidden-sm">
                <div class="hand-container">
                    <img class="iphone-hand img_res wow animated bounceInUp" data-wow-duration="1.2s" src="img/Assets.png" alt="">
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="headings">
                    <h2 class="wow animated fadeInDown">
                        <strong>New way to send your parcel.</strong>
                    </h2>
                    <p class="wow animated fadeInLeft">Locate, select, drop at a PopBox near you and PopSend will deliver. Join PopSend today for the PopBox experience!</p>

                    <p class="wow animated fadeInRight pad-top-10">Download Now</p>

                    <div class="row">
                        <div class="col-md-4">
                            <center><img class="img-responsive" src="img/qr.png" alt=""></center><br>
                        </div>
                        <div class="col-md-4">
                            <a href="#" class="btn btn-store wow animated bounceInUp">
                                <img class="img-responsive" src="img/download-play-store.png" alt="">
                            </a>
                            <a href="#" class="btn btn-store wow animated bounceInUp">
                                <img class="img-responsive" src="img/ios.png">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- ****************************** Video section ************************** -->
<section id="video" class="block overflow-visible">
    <div class="container">
        <div class="title-area text-center">
            <h1 class="section-title">VIdeo Preview</h1>
            <hr class="title-under-line-top" />
            <hr class="title-under-line-bottom" />
        </div>
        <div class="row clearfix video-area-wraper">
            <div class="col-md-8 forward-div">
                <div class="video-box">
                    <div class="video-play-btn">
                        <a class="ply-btn video" title="Lucy App"  href="../../../vimeo.com/123123.html">
                            <i class="ion-ios-play"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-md-push-4 video-caption">
                <div class="row caption-row">
                    <div class="col-md-6 col-md-push-6 caption-inner clearfix">
                        <div class="caption-content">
                            <p>
                                Use Lucy – the Best Responsive App Landing Page to embed your game’s or app’s review videos.app and game.
                            </p>
                            <a href="#" class="btn btn-default btn-download">Download Now <i class="ion-ios-arrow-thin-right icon-margin"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- ****************************** Big Features section ************************** -->
<section id="bigfeatures" class="img-block-3col block">

    <div class="container">

        <div class="title-area text-center">
            <h1 class="section-title">lucy features</h1>
            <hr class="title-under-line-top" />
            <hr class="title-under-line-bottom" />
        </div>


        <div class="row margin-top-50">
            <div class="col-sm-4 pad-right-85">
                <ul class="item-list-right item-list-big">
                    <li class="wow fadeInLeft animated">
                        <div style="display: inline-block;">
                            <div style="float: left;">
                                <h3 class="margin-top-right">Cloud Websites</h3>
                            </div>
                            <div class="test-2">
                                <div class="icon-test">
                                    <span class="x"><i class="ion-cloud"></i></span>
                                </div>
                            </div>
                        </div>
                        <p>Class aptent taciti sociosqu litora torquent conubia nos per inceptos himenaeos.</p>
                    </li>
                    <li class="wow fadeInLeft animated">
                        <div style="display: inline-block;">
                            <div style="float: left;">
                                <h3 class="margin-top-right">Cloud Websites</h3>
                            </div>
                            <div class="test-2">
                                <div class="icon-test">
                                    <span class="x"><i class="ion-android-color-palette"></i></span>
                                </div>
                            </div>
                        </div>
                        <p>Class aptent taciti sociosqu litora torquent conubia nos per inceptos himenaeos.</p>
                    </li>
                    <li class="wow fadeInLeft animated">
                        <div style="display: inline-block;">
                            <div style="float: left;">
                                <h3 class="margin-top-right">Cloud Websites</h3>
                            </div>
                            <div class="test-2">
                                <div class="icon-test">
                                    <span class="x"><i class="ion-android-hand"></i></span>
                                </div>
                            </div>
                        </div>
                        <p>Class aptent taciti sociosqu litora torquent conubia nos per inceptos himenaeos.</p>
                    </li>
                </ul>
            </div>
            <div class="col-sm-4 col-sm-push-4 pad-left-85">
                <ul class="item-list-left item-list-big">
                    <li class="wow fadeInRight animated condensed-icon">
                        <div style="display: inline-block;">
                            <div class="test-2">
                                <div class="icon-test">
                                    <span class="x"><i class="ion-android-unlock"></i></span>
                                </div>
                            </div>
                            <div style="float: left;">
                                <h3 class="margin-top-left">Cloud Websites</h3>
                            </div>
                        </div>
                        <p>Class aptent taciti sociosqu litora torquent conubia nos per inceptos himenaeos.</p>
                    </li>
                    <li class="wow fadeInRight animated condensed-icon">
                        <div style="display: inline-block;">
                            <div class="test-2">
                                <div class="icon-test">
                                    <span class="x"><i class="ion-arrow-resize"></i></span>
                                </div>
                            </div>
                            <div style="float: left;">
                                <h3 class="margin-top-left">Cloud Websites</h3>
                            </div>
                        </div>
                        <p>Class aptent taciti sociosqu litora torquent conubia nos per inceptos himenaeos.</p>
                    </li>
                    <li class="wow fadeInRight animated">
                        <div style="display: inline-block;">
                            <div class="test-2">
                                <div class="icon-test">
                                    <span class="x"><i class="ion-android-options"></i></span>
                                </div>
                            </div>
                            <div style="float: left;">
                                <h3 class="margin-top-left">Cloud Websites</h3>
                            </div>
                        </div>
                        <p>Class aptent taciti sociosqu litora torquent conubia nos per inceptos himenaeos.</p>
                    </li>
                </ul>
            </div>
            <div class="col-sm-4 col-sm-pull-4 text-center">
                <div class="animation-box wow bounceIn animated">
                    <img class="highlight-left wow animated" src="img/spark.png" height="192" width="48" alt="">
                    <img class="highlight-right wow animated" src="img/spark.png" height="192" width="48" alt="">
                    <img class="screen" src="img/03.png" alt="" height="581" width="300">
                </div>
            </div>
        </div>
    </div>
</section>


<!-- ****************************** Specialty section ************************** -->
<section id="features" class="block overflow-visible">
    <div class="container">
        <div class="title-area text-center">
            <h1 class="section-title">lucy speciality</h1>
            <hr class="title-under-line-top" />
            <hr class="title-under-line-bottom" />
        </div>

        <div class="row special-card-wraper">
            <div class="col-sm-6 col-md-3 special-card-outer">
                <div class="feature-box wow animated flipInX animated special-card text-center">
                    <div class="card-icon-box">
                        <div class="card-icon-box-inner">
                            <i class="ion-android-subway"></i>
                        </div>
                    </div>
                    <div class="card-content">
                        <h3 class="card-title">Multi Purpose</h3>
                        <p>Class aptent taciti sociosutn tora torquent conub nost reptos himenaeos.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 special-card-outer">
                <div class="feature-box wow animated flipInX animated special-card text-center">
                    <div class="card-icon-box">
                        <div class="card-icon-box-inner">
                            <i class="ion-ios-monitor"></i>
                        </div>
                    </div>
                    <div class="card-content">
                        <h3 class="card-title">Retina Ready</h3>
                        <p>Class aptent taciti sociosutn tora torquent conub nost reptos himenaeos.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 special-card-outer">
                <div class="feature-box wow animated flipInX animated special-card text-center">
                    <div class="card-icon-box">
                        <div class="card-icon-box-inner">
                            <i class="ion-arrow-resize"></i>
                        </div>
                    </div>
                    <div class="card-content">
                        <h3 class="card-title">Fully Responsive</h3>
                        <p>Class aptent taciti sociosutn tora torquent conub nost reptos himenaeos.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 special-card-outer">
                <div class="feature-box wow animated flipInX animated special-card text-center">
                    <div class="card-icon-box">
                        <div class="card-icon-box-inner">
                            <i class="ion-ios-gear"></i>
                        </div>
                    </div>
                    <div class="card-content">
                        <h3 class="card-title">Endless Possibilities</h3>
                        <p>Class aptent taciti sociosutn tora torquent conub nost reptos himenaeos.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>


<!-- ****************************** Left Image section ************************** -->
<section id="features-left-image" class="block">
    <div class="container">

        <div class="row">
            <div class="col-sm-6 wow fadeInLeft animated">
                <div class="phone-image">
                    <img class="img-responsive" src="img/Assets.png" alt="">
                </div>
            </div><!--/col-sm-6-->

            <div class="col-sm-6 wow fadeInRight animated">

                <div class="title-area text-center">
                    <h1 class="section-title">why lucy best</h1>
                </div>

                <div class="row">
                    <div class="col-md-12 causes-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ante ex, fermentum vel libero eget interdum semper libero. Curabitur egestas, arcu id tempor convallis.</p>
                        <ul class="feature-list">
                            <li><i class="ion-android-star"></i> Reliable and Secure Platform</li>
                            <li><i class="ion-android-star"></i> Everything is perfectly orgainized for future</li>
                            <li><i class="ion-android-star"></i> Attach large file easily</li>
                            <li><i class="ion-android-star"></i> Tons of features and easy to use and customize</li>
                        </ul>
                        <a href="#" class="btn btn-default btn-download">Download Now<i class="ion-ios-arrow-thin-right icon-margin"></i></a>
                    </div>
                </div>
            </div><!--/col-sm-6-->
            <div class="col-md-12 text-center bottom-img-wraper">
                <img src="img/iPhone%206%20-%20Gold%20copy.png" alt="">
            </div>
        </div><!--/row-->

    </div><!--/container-->
</section>


<!-- ****************************** Testimonial ************************** -->
<section id="testimonial" class="block overflow-visible">
    <div class="container">
        <div id="review" class="owl-carousel owl-theme wow animated bounceInUp">
            <div class="item">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                        <div class="client-pic"><img class="img_res" src="img/client-two.png" alt=""></div>
                        <p class="client-name">
                            Shahjahan Jewel
                        </p>
                        <p class="review-star">
                            <i class="ion-star"></i>
                            <i class="ion-star"></i>
                            <i class="ion-star"></i>
                            <i class="ion-star"></i>
                            <i class="ion-star-outline"></i>
                        </p>
                        <p class="review-desc">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip.
                        </p>
                        <p class="testimonial-date">January 28, 2016</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-sm-8 col-md-offset-2">
                        <div class="client-pic"><img class="img_res" src="img/client-three.png" alt=""></div>
                        <p class="client-name">
                            Jane Doe
                        </p>
                        <p class="review-star">
                            <i class="ion-star"></i>
                            <i class="ion-star"></i>
                            <i class="ion-star"></i>
                            <i class="ion-star"></i>
                            <i class="ion-star-outline"></i>
                        </p>
                        <p class="review-desc">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip.
                        </p>
                        <p class="testimonial-date">January 28, 2016</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ******************************* Tweets Divider ************************ -->
<section id="tweets">
    <div class="container">
        <div class="ticker1 text-center">
            <div class="innerWrap ticker-wraper">
                <div class="list">
                    <i class="ion-social-twitter"></i><span clas="name-who-tweets">Leone Messi :</span><span class="tweets-content"> This is the best post</span>
                </div>
                <div class="list">
                    <i class="ion-social-twitter"></i><span clas="name-who-tweets">Leone Messi :</span><span class="tweets-content"> This is the best post</span>
                </div>
                <div class="list">
                    <i class="ion-social-twitter"></i><span clas="name-who-tweets">Leone Messi :</span><span class="tweets-content"> This is the best post</span>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ****************************** Team section ************************** -->
<section id="team" class="block">
    <div class="container">
        <div class="title-area text-center">
            <h1 class="section-title">who developed lucy</h1>
            <hr class="title-under-line-top" />
            <hr class="title-under-line-bottom" />
        </div>
        <div class="row developer-area-wraper">
            <div class="col-md-6 col-sm-12">
                <div class="row developer-card">
                    <div class="col-md-6 col-sm-6 no-pad-left-right">
                        <img class="img-res" src="img/Chairpersons_2.png" alt="">
                    </div>
                    <div class="col-md-6 col-sm-6 text-center developer-desc">
                        <p class="developer-name">Smith Mehra</p>
                        <p class="developer-designation">Graphics Designer</p>
                        <ul class="team-social">
                            <li class="wow animated fadeInLeft facebook"><a href="#"><i class="ion-social-facebook"></i></a></li>
                            <li class="wow animated fadeInLeft linkedin"><a href="#"><i class="ion-social-linkedin"></i></a></li>
                            <li class="wow animated fadeInRight googleplus"><a href="#"><i class="ion-social-googleplus-outline"></i></a></li>
                            <li class="wow animated fadeInRight github"><a href="#"><i class="ion-social-github"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="row developer-card">
                    <div class="col-md-6 col-sm-6 no-pad-left-right">
                        <img class="img-res" src="img/Chairpersons_3.png" alt="">
                    </div>
                    <div class="col-md-6 col-sm-6 text-center developer-desc">
                        <p class="developer-name">Wiliams White</p>
                        <p class="developer-designation">Apps Developer</p>
                        <ul class="team-social">
                            <li class="wow animated fadeInLeft facebook"><a href="#"><i class="ion-social-facebook"></i></a></li>
                            <li class="wow animated fadeInLeft linkedin"><a href="#"><i class="ion-social-linkedin"></i></a></li>
                            <li class="wow animated fadeInRight googleplus"><a href="#"><i class="ion-social-googleplus-outline"></i></a></li>
                            <li class="wow animated fadeInRight github"><a href="#"><i class="ion-social-github"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="row developer-card">
                    <div class="col-md-6 col-sm-6 no-pad-left-right">
                        <img class="img-res" src="img/Chairpersons_1.png" alt="">
                    </div>
                    <div class="col-md-6 col-sm-6 text-center developer-desc">
                        <p class="developer-name">Snikda Chacra</p>
                        <p class="developer-designation">Apps Developer</p>
                        <ul class="team-social">
                            <li class="wow animated fadeInLeft facebook"><a href="#"><i class="ion-social-facebook"></i></a></li>
                            <li class="wow animated fadeInLeft linkedin"><a href="#"><i class="ion-social-linkedin"></i></a></li>
                            <li class="wow animated fadeInRight googleplus"><a href="#"><i class="ion-social-googleplus-outline"></i></a></li>
                            <li class="wow animated fadeInRight github"><a href="#"><i class="ion-social-github"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="row developer-card">
                    <div class="col-md-6 col-sm-6 no-pad-left-right">
                        <img class="img-res" src="img/tm2.png" alt="">
                    </div>
                    <div class="col-md-6 col-sm-6 text-center developer-desc">
                        <p class="developer-name">Alia Nehra</p>
                        <p class="developer-designation">Apps Developer</p>
                        <ul class="team-social">
                            <li class="wow animated fadeInLeft facebook"><a href="#"><i class="ion-social-facebook"></i></a></li>
                            <li class="wow animated fadeInLeft linkedin"><a href="#"><i class="ion-social-linkedin"></i></a></li>
                            <li class="wow animated fadeInRight googleplus"><a href="#"><i class="ion-social-googleplus-outline"></i></a></li>
                            <li class="wow animated fadeInRight github"><a href="#"><i class="ion-social-github"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- ****************************** Pricing section ************************** -->
<section id="pricing" class="block">
    <div class="container">
        <div class="title-area text-center">
            <h1 class="section-title">Lucy Price plan</h1>
            <hr class="title-under-line-top" />
            <hr class="title-under-line-bottom" />
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="pricing-table">
                    <li class="wow flipInY animated" style="visibility: visible;">
                        <h3>Standard</h3>
                        <ul class="benefits-list">
                            <li>Responsive</li>
                            <li>Documentation</li>
                            <li class="not">Multiplatform</li>
                            <li class="not">Video background</li>
                            <li class="not">Support</li>
                            <li class="special-price">from <strong>$2.99</strong> /month</li>
                        </ul>
                        <a href="#" class="btn btn-default btn-download">get started now</a>
                    </li>
                    <li class="silver wow flipInY animated" data-wow-delay="0.2s" style="visibility: visible;-webkit-animation-delay: 0.2s; -moz-animation-delay: 0.2s; animation-delay: 0.2s;">
                        <h3>Sliver</h3>
                        <ul class="benefits-list">
                            <li>Responsive</li>
                            <li>Documentation</li>
                            <li>Multiplatform</li>
                            <li class="not">Video background</li>
                            <li class="not">Support</li>
                            <li class="special-price">from <strong>$4.99</strong> /month</li>
                        </ul>
                        <a href="#" class="btn btn-default btn-download">get started now</a>
                    </li>

                    <li class="gold wow flipInY animated" data-wow-delay="0.4s" style="visibility: visible;-webkit-animation-delay: 0.4s; -moz-animation-delay: 0.4s; animation-delay: 0.4s;">
                        <h3>Gold</h3>
                        <ul class="benefits-list">
                            <li>Responsive</li>
                            <li>Documentation</li>
                            <li>Multiplatform</li>
                            <li>Video background</li>
                            <li>Support</li>
                            <li class="special-price">from <strong>$7.99</strong> /month</li>
                        </ul>
                        <a href="#" class="btn btn-default btn-download">get started now</a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</section>


<!-- ****************************** Subscribe section ************************** -->
<section id="subscribe" class="block">
    <div class="container">
        <div class="title-area text-center">
            <h1 class="section-title">what's new</h1>
            <hr class="title-under-line-top" />
            <hr class="title-under-line-bottom" />
        </div>
        <div class="row subscription-wraper">
            <div class="col-md-6 ">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ante ex, fermentum vel libero eget interdum semper libero. Curabitur egestas, arcu id tempor convallis.</p>
                <ul class="feature-list subscription-ul">
                    <li><i class="ion-android-star"></i> Reliable and Secure Platform</li>
                    <li><i class="ion-android-star"></i> Everything is perfectly orgainized for future</li>
                    <li><i class="ion-android-star"></i> Attach large file easily</li>
                    <li><i class="ion-android-star"></i> Tons of features and easy to use and customize</li>
                </ul>
            </div>
            <div class="col-md-6 subscription-area">
                <div class="subscription-content">
                    <p class="title-text">Subscribe</p>
                    <p>Get updates directly to your inbox</p>
                </div>
                <form id="subscription-form" class="subscription-form" role="search">
                    <div class="input-group add-on">
                        <input type="email" class="form-control subscription-form-control subscriber-email" placeholder="Email" name="srch-term" id="srch-term">
                        <div class="input-group-btn">
                            <button id="subscribe-button" class="btn btn-default" type="submit"><i class="ion-ios-paperplane"></i></button>
                        </div>
                    </div>
                </form>

                <!-- SUCCESS OR ERROR MESSAGES -->
                <div id="subscription-response" class="subscription-success"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 wow animated bounceInUp">

            </div>
        </div>
    </div>
</section>


<!-- ****************************** Contact section ************************** -->
<section id="contact" class="block">
    <div class="container">
        <div class="title-area text-center">
            <h1 class="section-title">Get In Touch Say Hello!</h1>
            <hr class="title-under-line-top" />
            <hr class="title-under-line-bottom" />
        </div>
        <div class="contact-introductory-text text-center col-md-8 col-md-offset-2">
            This was a awesome app. I deleted all the others and kept only this one. But this update you just done ruined it, can't do anything with this. Went from the best to the worst, guess deleting it now. I will never understand when something's good always changing things and messing it up. Geez

        </div>

        <div class="row">
            <div id="response" class="col-md-11 col-md-offset-1"></div>
            <form id="contact_form" action="{{ url('/register') }}" method="post">
                <div class="col-md-5 col-md-offset-1">
                    <div class="form-group">
                        <input type="name" class="form-control height-50" id="name" name="name" placeholder="Name" required>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control height-50" id="email" name="email" placeholder="Email" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <textarea class="form-control" placeholder="Message" name="message" id="message"></textarea>
                </div>
                <div class="col-md-12 text-center submit-btn-wraper">
                    <button type="submit" name="submit" id="submit" class="btn btn-default btn-download btn-contact">send message<i class="ion-ios-arrow-thin-right icon-margin"></i></button>
                </div>
            </form>
        </div><!--/row-->
    </div>
    <div class="clearfix"></div>
</section>


<!-- ****************************** Footer ************************** -->
<section id="footer" class="block">
    <div class="container text-center">
        <div class="footer-logo">
            <a class="logo" href="#"><img src="img/logo-dark.png" alt=""></a>
        </div>
        <div class="copyright">
            &copy 2018 All Rights Reserved by PopBox Asia Services
        </div>
    </div><!-- container -->
</section>


<!-- ****************************** Back to top ************************** -->
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left">
    <span class="glyphicon glyphicon-chevron-up"></span>
</a>


<!-- All the scripts -->
<script src="{{ asset('libs/jquery/1.11.1/jquery.min.js') }}"></script>
<script src="{{ asset('libs/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/wow.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.js') }}"></script>
<script src="{{ asset('js/nivo-lightbox.min.js') }}"></script>
<script src="{{ asset('js/smoothscroll.js') }}"></script>
<script src="{{ asset('js/jquery.ajaxchimp.min.js') }}"></script>
<script src="{{ asset('js/script.js') }}"></script>
<script src="{{ asset('js/contact.js') }}"></script>
<script src="{{ asset('js/jquery.easy-ticker.js') }}"></script>
<script src="{{ asset('js/jquery.magnific-popup.js') }}"></script>

</body>
</html>