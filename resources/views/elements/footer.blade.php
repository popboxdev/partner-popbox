<section id="footer" class="block">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <img src="{{ url('/img/logo-dark.png') }}" class="img-responsive"><br>

                <ul class="footer-block">
                    <li>
                        <ul class="footer-address">
                            <li><img src="{{ url('/img/phone.png') }}" class="img-responsive"></li>
                            <li><a href="tel:+622122538719">+62 21 22538719</a> Indonesia</li><br>
                            <li style="padding-left: 39px;"><a href="tel:+6001110606011">+60 0111 0606 011</a> Malaysia</li>
                        </ul>
                    </li>
                    <li>
                        <ul class="footer-address">
                            <li><img src="{{ url('/img/mail.png') }}" class="img-responsive"></li>
                            <li><a href="mailto:@lang('landing.email')" target="_top">@lang('landing.email')</a></li>
                        </ul>
                    </li>
                    <li>
                        <ul class="footer-address">
                            <li><img src="https://popsend.popbox.asia/img/address.png" class="img-responsive"></li><li class="address-resize">Jl. Palmerah Utara III no 62 FGH Palmerah<br>Jakarta Barat 11480 - Indonesia</li>
                        </ul>
                    </li>
                    </li>
                    <li class="mar-top-30">&copy 2018 All Rights Reserved by PopBox Asia Services</li>
                </ul>
            </div>
            <div class="col-md-7 pull-right">
                <div class="row">
                    <div class="col-md-12">
                        <img src="{{ url('/img/footer.png') }}" class="img-responsive">
                    </div>
                    <div class="col-md-12 mar-top-100">
                        <ul class="social-media-list">
                            <li><a href="http://line.me/ti/p/~@popbox_asia" onclick="clickSocials(5)" target="_blank"><img src="{{ url('/img/line.png') }}" class="img-responsive"></a></li>
                            <li><a href="https://twitter.com/popbox_asia" onclick="clickSocials(4)" target="_blank"><img src="{{ url('/img/twitter.png') }}" class="img-responsive"></a></li>
                            <li><a href="https://www.youtube.com/channel/UCYGHLg4Xdc_IHD5DHdDIRTA" onclick="clickSocials(3)" target="_blank"><img src="{{ url('/img/youtube.png') }}" class="img-responsive"></a></li>
                            <li><a href="https://www.instagram.com/popbox_asia/" onclick="clickSocials(2)" target="_blank"><img src="{{ url('/img/ig.png') }}" class="img-responsive"></a></li>
                            <li><a href="https://www.facebook.com/pboxasia/" onclick="clickSocials(1)" target="_blank"><img src="{{ url('/img/fb.png') }}" class="img-responsive"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="@lang('landing.backtotop')" data-toggle="tooltip" data-placement="left">
    <span class="glyphicon glyphicon-chevron-up"></span>
</a>