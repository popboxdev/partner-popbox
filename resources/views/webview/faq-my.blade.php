<!DOCTYPE html>
<html>
<head>
	<title>PopSend - FAQ</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>PopSend by PopBox</title>
	<meta name="description" content="Popbox Popsend">
	<meta name="author" content="Popbox Asia">
	<meta property="og:url" content="{{ url('/') }}" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="PopSend By PopBox" />
	<meta property="og:description" content="New way to send your parcel. Locate, select, drop at a PopBox near you and PopSend will deliver. Join PopSend today for the PopBox experience!" />
	<meta property="og:image" content="{{ URL::asset('img/meta-img.png')}}" />

	<link rel="shortcut icon" href="{{ asset('/img/favico128.ico') }}" type="image/x-icon">
	<link rel="icon" href="{{ asset('/img/favico128.ico') }}" type="image/x-icon">

	<link rel="stylesheet" type="text/css"  href="{{ URL::asset('css/bootstrap.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('font-awesome/css/font-awesome.css')}}">
	<link rel="stylesheet" type="text/css"  href="{{ URL::asset('css/style2.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/prettyPhoto.css')}}">
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'>

	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script type="text/javascript" src="{{ URL::asset('js/bootstrap.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/sha1.min.js')}}"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script type="text/javascript" src="{{ URL::asset('js/script.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/jquery.qrcode.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/qrcode.js')}}"></script>
</head>
<body>
	<div class="container">
		<img src="{{ asset('img/faq.png') }}" class="img-responsive">
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingOne">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							What is PopSend?
						</a>
					</h4>
				</div>
				<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
					<div class="panel-body">
						PopSend application allows users to book deliveries and other O2O services (laundry, leather treatment etc). By utilizing the app, each Parcel Locker will serve as a personal mailbox for users to send and receive items. Simply select a preferred locker to drop your parcel and PopBox will get it delivered the following day! You can send the parcel to address or another locker.
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingTwo">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							How do I make an order?​
						</a>
					</h4>
				</div>
				<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
					<div class="panel-body">
						1. Enter the drop off locker and delivery address.<br>
						2. Enter the recipient detail and item details, and item photo.<br>
						3. Confirm the order.<br>
						4. You will receive the shipping label, please stick it to the parcel.<br>

					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingTwoOne">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwoOne" aria-expanded="false" aria-controls="collapseTwoOne">
							How do i drop my parcel to locker?
						</a>
					</h4>
				</div>
				<div id="collapseTwoOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwoOne">
					<div class="panel-body">
						1. Choose language preferences<br>
						2. Choose "send item/mengirim barang"<br>
						3. Scan the QR code/type manually the order code with "PLAxxxxx" code<br>
						4. Choose size and input the parcel inside locker<br>

					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingThree">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							​​Can I track my package?
						</a>
					</h4>
				</div>
				<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
					<div class="panel-body">
						​​Yes, and it is available in the history menu.
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingThree">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
							​​How long does it take for an order to be delivered?
						</a>
					</h4>
				</div>
				<div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
					<div class="panel-body">
						​​Operational delivery time is Monday - Saturday. <br>
						Delivery time for <strong>Klang Valley</strong> area is 1-2 working days. <br>
						Delivery time for areas outside of <strong>Klang Valley</strong> will take 2-4 working days depending on the location. <br>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingThree">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
							​​What are the shipping costs?
						</a>
					</h4>
				</div>
				<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
					<div class="panel-body">
						​​Shipping cost depends on the weight, you may check the tariff on fare estimate menu.
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingThree">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
							​​What is the allowed weight​ ​ for each package?
						</a>
					</h4>
				</div>
				<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
					<div class="panel-body">
						​​The allowed weight limit is 5kg maximum (on reference to th payment page where there are options for up to 5kg)
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingThreeOne">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThreeOne" aria-expanded="false" aria-controls="collapseThreeOne">
							What are the prohibited items in the service?
						</a>
					</h4>
				</div>
				<div id="collapseThreeOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThreeOne">
					<div class="panel-body">
						1. Food/drinks <br>
						2. Live animals/plants <br>
						3. Dangerous goods, flammable goods, explosive items, weapons, etc <br>
						4. Drugs/narcotics and anything that is prohibited by Indonesian Government <br>
						5. Valuable items (jewelry, valuable certificates, etc) <br>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingThree">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
							​​How can I contact PopBox for the service?
						</a>
					</h4>
				</div>
				<div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
					<div class="panel-body">
						​​Please call us on  011-1060 6011 or message to info@popbox.asia. <br>
						Customer service hours: Monday - Friday 9AM - 6PM <br>
						For quick reply and response,  please send us a personal message on our facebook page.
					</div>
				</div>
			</div>

		</div>
	</div>
</body>
</html>
