<p align="justify">
    <strong> </strong>
</p>
<p align="justify">
    <strong> </strong>
</p>
<p align="justify">
    <strong>Syarat &amp; ketentuan penggunaan Popbox</strong>
    <strong></strong>
</p>
<p align="justify">
    <strong>1. Definisi</strong>
    <strong></strong>
</p>
<p align="justify">
    PopSend adalah aplikasi layanan jasa pengiriman barang dari/ke locker
    PopBox dimana pelanggan dapat melakukan proses order pengiriman via
    aplikasi, memasukkan barang via locker popbox dan barang akan dikirimkan ke
    locker lainnya atau alamat penerima
</p>
<p align="justify">
    <strong>2. Barang yang tidak bisa dikirim ke/dari locker PopBox</strong>
    <strong></strong>
</p>
<p align="justify">
    Pengguna PopBox locker dalam pengiriman barang, akan mematuhi seluruh
    ketentuan dan peraturan dalam penggunaan serta memastikan keamanan
    barang-barang yang dikirimkan ke PopBox locker demi menjaga keamanan dan
    kenyamanan bagi seluruh pihak yaitu:
</p>
<p align="justify">
    Berikut adalah barang-barang yang dilarang untuk dikirim menggunakan PopBox
    Locker:
</p>
<p align="justify">
    - uang (dalam bentuk kertas maupun benda berharga lainnya)
</p>
<p align="justify">
    - narkotika dan segala substansi psikoatif lainnya
</p>
<p align="justify">
    - benda berharga (barang antik, dll)
</p>
<p align="justify">
    - binatang, tanaman atau lainnya
</p>
<p align="justify">
    - benda-benda pornografi
</p>
<p align="justify">
    - barang yang segar (buah, sayur, daging, lainnya)
</p>
<p align="justify">
    - barang pecah belah
</p>
<p align="justify">
    - senjata, amunisi, benda lainnya yang mudah terbakar/meledak
</p>
<p align="justify">
    - barang yang basah, bocor, menimbulkan bau dan lainnya
</p>
<p align="justify">
    - barang yang dapat menyebabkan resiko kesehatan, kerusakan dan lainnya
</p>
<p align="justify">
    - barang lainnya yang dilarang sesuai oleh peraturan Undang-Undang Republik
    Indonesia
</p>
<p align="justify">
    - barang yang memerlukan ijin khusus dalam pengiriman
</p>
<p align="justify">
    Pelanggaran atas penyalahgunaan jasa PopBox atas barang-barang yang
    dilarang diatas menjadi tanggung jawab dari pengirim secara sepenuhnya dan
    harus mengganti rugi secara materil dan imateril atas segala kerugian yang
    timbul.
</p>
<p align="justify">
    <strong>3. Hak &amp; Kewajiban dari PopBox Asia</strong>
    <strong></strong>
</p>
<p align="justify">
    - Melakukan pengiriman sesuai dengan detail data pengiriman yang telah
    diberikan oleh pelanggan yang tertera dalam aplikasi
</p>
<p align="justify">
    - Menunjuk partner logistik ketiga untuk melakukan penjemputan atau
    pengiriman barang
</p>
<p align="justify">
    - Mengambil langkah yang diperlukan untuk menghindari adanya penggunaan
    locker secara tidak benar dimana bisa merugikan pihak Pemilik Online Store,
    Pihak PopBox Asia, Pemilik lokasi maupun konsumer.
</p>
<p align="justify">
    - Membuka box kiriman/menutup akses untuk mengambil barang apabila diminta
    oleh pihak Pemerintah/Kepolisian yang berwenang tanpa seijin pemilik barang
    ataupun pengirim
</p>
<p align="justify">
    - PopBox dibebaskan dari segala klaim, kerugian yang timbul akibat
    penyalahgunaan jasa service oleh pelanggan dan segala kerugian menjadi
    tanggung jawab pengirim sepenuhnya
</p>
<p align="justify">
    <strong> </strong>
</p>
<p align="justify">
    <strong>4. Detail Ketentuan Pengiriman Popbox</strong>
    <strong></strong>
</p>
<p align="justify">
    1. Pengirim harus memberikan deskripsi yang benar mengenai produk yang
    dikirimkan dan data pengiriman yang benar dan lengkap
</p>
<p align="justify">
    2. Pengirim harus menempelkan / menulis dengan jelas detail nomor order dan
    tujuan pengiriman
</p>
<p align="justify">
    3. Berat barang tidak boleh lebih 3 (tiga) Kilogram (Kg)
</p>
<p align="justify">
    4. Ukuran barang tidak boleh melebihi ukuran loker yang tersedia
</p>
<p align="justify">
    5. Untuk saat ini pengiriman locker ke locker popbox hanya untuk area
    Jabodetabek, untuk ke alamat diluar area Jabodetabek bisa menggunakan
    Pengiriman Locker ke Alamat. Penambahan area layanan akan diberitahukan
    kemudian
</p>
<p align="justify">
    6. Data pengiriman harus lengkap. Apabila data tidak lengkap / salah yang
    merupakan kelalaian dari pengirim, maka bukan tanggung jawab dari pihak
    PopBox
</p>
<p align="justify">
    7. Pengambilan barang hanya pada hari
    <strong>
        Senin sampai dengan Sabtu dari pukul 08.00 – 17.00 WIB dan Sabtu 08.00
        - 12.00
    </strong>
    dan tidak beroperasi pada hari Minggu dan Libur Nasional
</p>
<p align="justify">
    8. Estimasi pengiriman barang adalah H+2-3 hari kerja setelah barang
    dimasukkan ke dalam locker untuk area Jabodetabek.
</p>
<p align="justify">
    9. PopBox berhak untuk menolak pengiriman. Popbox berhak membuka
    loker/paket jika barang dianggap mencurigakan / diminta oleh otoritas
    berwenang.
</p>
<p align="justify">
    <strong>
        5. Asuransi/Kehilangan/Kerusakkan barang dan keterlambatan pengiriman
    </strong>
    <strong></strong>
</p>
<p align="justify">
    - Segala pengiriman barang yang tidak sesuai dengan ketentuan pengiriman
    PopBox yang menyebabkan kerugian / kerusakan pada pihak tertentu, baik
    pihak pengirim, penerima, PopBox, ataupun pihak lainnya, tanggung jawab
    sepenuhnya berada di pihak pengirim.
</p>
<p align="justify">
    - Barang tidak dikirimkan oleh kurir PopBox apabila kondisi kemasan paket
    rusak atau tidak dikemas dengan baik. Pihak PopBox tidak akan bertanggung
    jawab atas kelalaian yang disebabkan oleh pihak pengirim
</p>
<p align="justify">
    - Kehilangan/kerusakkan barang yang disebabkan oleh kelalaian dari pihak
    PopBox dapat diklaim asuransi sebesar 10x biaya pengiriman dan klaim harus
    diinformasikan paling lambat 3 (tiga) hari kerja secara resmi dan tertulis
    setelah barang diterima oleh pelanggan. Apabila melewat batas klaim, maka
    kerusakkan tidak dapat di klaim lagi ke Pihak PopBox
</p>
<p align="justify">
    - Apabila terjadi keterlambatan pengiriman, PopBox akan memberikan
    informasi kepada pelanggan mengenai kendala yang dihadapi dan barang akan
    tetap dikirim dalam kurun waktu secepat-cepatnya
</p>
<p align="justify">
    <strong>6. Pengiriman yang gagal</strong>
    <strong></strong>
</p>
<p align="justify">
    Atas kondisi dibawah ini, pihak PopBox berhak untuk melakukan pemusnahan
    barang tanpa konfirmasi ke Pihak pelanggan:
</p>
<p align="justify">
    - PopSend hanya melakukan pengiriman ulang maksimal sebanyak 3 (tiga) kali
    apabila penerima tidak ada ditempat ataupun tidak dapat dihubungi.
    Setelahnya barang bisa dikirimkan kembali ke lokasi locker pengirim atau
    diambil sendiri oleh Penerima di lokasi kantor PopBox.
</p>
<p align="justify">
    - Barang yang menyalahgunakan ketentuan pemakaian jasa pengiriman PopSend
    (contoh: makanan yang memiliki kadaluarsa, berbahaya, dll)
</p>
<p align="justify">
    - Pelanggan tidak melakukan pembayaran atas jasa pengiriman barang
</p>
<p align="justify">
    - Segala barang yang tidak diambil, tidak dapat dikirimkan, tidak dapat
    dihubungi baik pihak pengirim maupun penerima dalam waktu lebih dari 14
    hari kalender
</p>
<p align="justify">
    <strong>7. Hubungi kami</strong>
    <strong></strong>
</p>
<p align="justify">
    Syarat dan ketentuan dapat berubah sewaktu-waktu tanpa pemberitahuan. Untuk
    informasi lebih lanjut dapat hubungi kami di 021- 2122538719 atau
    info@popbox.asia (Senin - Jumat: 08.00 - 17.00 dan Sabtu 08.00 - 14.00)
</p>
<p align="justify">
    Hal-hal yang belum diatur atau belum cukup diatur dalam Syarat dan
    Ketentuan ini, penyelesaian pengaturannya akan diputuskan oleh dan atas
    permufakatan bersama dari Para Pihak.
</p>