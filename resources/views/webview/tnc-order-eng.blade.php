<p align="justify">
    <strong> </strong>
</p>
<p align="justify">
    <strong> </strong>
</p>
<p align="justify">
    <strong> </strong>
</p>
<p align="justify">
    <strong>Popbox terms &amp; conditions of use</strong>
    <strong></strong>
</p>
<p align="justify">
    <strong> </strong>
</p>
<p align="justify">
    <strong>1. Definitions</strong>
    <strong></strong>
</p>
<p align="justify">
    PopSend application allows users to book deliveries and other O2O services
    (laundry, leather treatment etc). By utilizing the app, each Parcel Locker
    will serve as a personal mailbox for users to send and receive items.
    Simply select a preferred locker to drop your parcel and PopBox will get it
    delivered the following day! You can send the parcel to address or another
    locker.
</p>
<p align="justify">
    <strong>2. Items that can not be sent to / from the PopBox locker</strong>
    <strong></strong>
</p>
<p align="justify">
    Users of PopBox locker in the delivery of goods, will comply with all terms
    and regulations in the use and ensure the security of goods delivered to
    the PopBox locker in order to maintain security and convenience for all
    parties, namely:
</p>
<p align="justify">
    The following items are prohibited to be sent using PopBox Locker:
</p>
<p align="justify">
    - money (in paper or other valuable items)
</p>
<p align="justify">
    - narcotics and any other psycho-legal substance
</p>
<p align="justify">
    - valuable items (antiques, etc.)
</p>
<p align="justify">
    - animals, plants or other
</p>
<p align="justify">
    - pornographic objects
</p>
<p align="justify">
    - fresh goods (fruits, vegetables, meat, other)
</p>
<p align="justify">
    - glassware
</p>
<p align="justify">
    - weapons, ammunition, other flammable / explosive objects
</p>
<p align="justify">
    - wet, leaking, stinking and other items
</p>
<p align="justify">
    - goods that may cause health, damage and other risks
</p>
<p align="justify">
    - other goods which are prohibited in accordance with the laws of the
    Republic of Indonesia
</p>
<p align="justify">
    - goods that require special permit in shipment
</p>
<p align="justify">
    Violation of misuse of the services of PopBox on the forbidden goods above
    shall be the sole responsibility of the sender completely and shall
    indemnify material and immaterial for any damages arising.
</p>
<p align="justify">
    <strong>3. Rights &amp; Duties of PopBox Asia</strong>
    <strong></strong>
</p>
<p align="justify">
    - Make delivery in accordance with the details of delivery data that has
    been given by the customer listed in the application
</p>
<p align="justify">
    - Appoint a third logistics partner to pick up or deliver goods
</p>
<p align="justify">
    - Take the necessary steps to avoid unauthorized use of locker which may
    harm the Online Store Owner, PopBox Asia Party, the owner of the location
    or the consumer.
</p>
<p align="justify">
    - Opening the shipment box / close access to pick up the goods if requested
    by the Government / Police authorities without the permission of the owner
    of goods or shippers
</p>
<p align="justify">
    - PopBox is exempt from any claims, losses arising from the misuse of the
    service by the customer and any losses shall be the sole responsibility of
    the sender
</p>
<p align="justify">
    <strong> </strong>
</p>
<p align="justify">
    <strong>4. Details of Popbox Delivery Requirements</strong>
    <strong></strong>
</p>
<p align="justify">
    <strong> </strong>
</p>
<p align="justify">
    1. The sender shall provide a correct description of the delivered product
    and the correct and complete shipping data
</p>
<p align="justify">
    2. The sender must attach / write clearly the details of the order number
    and delivery destination
</p>
<p align="justify">
    3. Weight of goods shall not exceed 3 (three) Kilograms (Kg)
</p>
<p align="justify">
    4. The size of the goods should not exceed the size of available lockers
</p>
<p align="justify">
    5. For now the delivery of locker to locker only for Jabodetabek area, to
    address outside Jabodetabek area can use Shipping Locker to Address. The
    addition of service area will be notified later
</p>
<p align="justify">
    6. Shipment data must be complete. If the data is incomplete / wrong which
    is the default of the sender, then it is not the responsibility of the
    PopBox party
</p>
<p align="justify">
    7. Taking goods only on Monday to Saturday from 08.00 - 17.00 WIB and
    Saturday 08.00 - 12.00 and not operating on Sunday and National Holidays
</p>
<p align="justify">
    8. Estimated delivery of goods is H + 2-3 working days after goods put into
    locker for Jabodetabek area.
</p>
<p align="justify">
    9. PopBox reserves the right to refuse delivery. Popbox reserves the right
    to open a locker / package if the goods are deemed suspicious / requested
    by the competent authorities.
</p>
<p align="justify">
    <strong> </strong>
</p>
<p align="justify">
    <strong>5. Insurance / Lost / Damage of goods and delivery delay</strong>
    <strong></strong>
</p>
<p align="justify">
    - Any delivery of goods not in accordance with the terms of delivery of
    PopBox which cause damage to certain parties, either the sender, the
    recipient, the PopBox, or any other party, the sole responsibility is on
    the sending party.
</p>
<p align="justify">
    - Items are not shipped by the PopBox courier when packaged package
    conditions are damaged or not packaged properly. Parties PopBox will not be
    responsible for negligence caused by the sender
</p>
<p align="justify">
    - Loss / damage of goods caused by negligence from PopBox party can be
    claimed by insurance for 10x shipping cost and claim must be informed no
    later than 3 (three) working days officially and in writing after the goods
    are received by the customer. If it passes the claim limit, then damages
    can not be claimed again to the PopBox Party
</p>
<p align="justify">
    - In the event of any delivery delays, PopBox will inform the customer
    about the constraints faced and the goods will remain in the shortest
    possible time
</p>
<p align="justify">
    <strong> </strong>
</p>
<p align="justify">
    <strong>6. Delivered delivery</strong>
    <strong></strong>
</p>
<p align="justify">
    <strong> </strong>
</p>
<p align="justify">
    Under the conditions below, the PopBox reserves the right to perform the
    destruction of goods without confirmation to the Customer:
</p>
<p align="justify">
    - PopSend only resend a maximum of 3 (three) times if the recipient is not
    in place or can not be contacted. Afterwards the goods can be shipped back
    to the sender's locker location or taken by the Recipient at the PopBox
    office location.
</p>
<p align="justify">
    - Goods that misuse the terms of use of PopSend delivery services (eg. food
    that has expired, dangerous, etc.)
</p>
<p align="justify">
    - Customers do not make payments for goods delivery services
</p>
<p align="justify">
    - Any items not taken, not shipped, can not be contacted either by sender
    or recipient within more than 14 calendar days
</p>
<p align="justify">
    <strong> </strong>
</p>
<p align="justify">
    <strong>7. Contact us</strong>
    <strong></strong>
</p>
<p align="justify">
    Terms and conditions are subject to change without notice. For further
    information please contact us at 021- 2122538719 or info@popbox.asia
    (Monday - Friday: 08.00 - 17.00 and Saturday 08.00 - 14.00)
</p>
<p align="justify">
    <strong> </strong>
</p>
<p align="justify">
    <strong> </strong>
</p>
<p align="justify">
    <strong> </strong>
</p>
<p align="justify">
    <strong> </strong>
</p>
<p align="justify">
    Matters that have not been regulated or sufficiently regulated in these
    Terms and Conditions, the settlement of arrangements shall be decided by
    and on the mutual consent of the Parties.
</p>