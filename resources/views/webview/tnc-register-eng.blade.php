<p>
    <strong>User Terms and Conditions</strong>
    <strong></strong>
</p>
<p>
    By downloading, installing and / or using the POPBOX (Popsend) application,
    you agree that you have read, understood and accepted and agreed to these
    Terms and Conditions ("Terms and Conditions of Use"). These Terms of Use
    constitute a legal agreement between you and PT Popbox Asia Services and
    the Services and Applications (as defined below).
</p>
<p>
    Please cancel your account (if you have signed up for the Application) and
    permanently remove the app from your device if you do not agree or do not
    want to enter the Terms of Use.
</p>
<p>
    PLEASE READ YOUR PRIVACY USE AND PRIVACY POLICY CAREFULLY BEFORE
    DOWNLOADING THE APPLICATION OR USING OUR SERVICE FOR THE FIRST TIME.
</p>
<p>
    <strong>1. General Things</strong>
    <strong></strong>
</p>
<p>
    1. PT. Popbox Asia Services is a company incorporated under the laws of the
    Republic of Indonesia.
</p>
<p>
    2. This application is a software application that serves as a means to
    make parcel delivery using Smart Locker Popbox available in various
    strategic places. The types of services that can be used through the app
    are:
</p>
<p>
    1. Delivery of Parcel
</p>
<p>
    2. Custody of Parcel.
</p>
<p>
    3. Track &amp; Delivery history of Parcel
</p>
<p>
    4. Refill the balance / Top Up Balance
</p>
<p>
    5. Sevice On Demand
</p>
<p>
    6. Other services we may add from time to time.
</p>
<p>
    <strong>2. Terms For Using the Application</strong>
    <strong></strong>
</p>
<p>
    1. You represent and warrant that you are an individual who is legally
    entitled to enter into binding agreements under the laws of the Republic of
    Indonesia, in particular the Terms of Use, to use the Application and that
    you are at least 17 years of age or married. Otherwise, we or the
    applicable Service Provider, are entitled under the law to cancel any
    agreements made with you. You further represent and warrant that you have
    the right, authority and capacity to use the Service and to comply with the
    Terms of Use of the Service and the Application.
</p>
<p>
    2. We collect and process your personal information, such as your name,
    email address (e-mail), and your mobile phone number when you register. You
    must provide accurate and complete information, update the information and
    agree to provide us with any proof of identity that we can reasonably
    request. If the personal information you provide to us is subject to
    change, for example, if you change your email address, phone number, or if
    you wish to cancel your account, please update your details by sending your
    request to us. We will enforce the requested changes.
</p>
<p>
    3. You may only use the Application when you have registered on the
    Application. Once you have successfully registered, the Application will
    provide you with a personal account accessible with the password you
    choose.
</p>
<p>
    4. Only You may use your own account and you promise not to authorize
    others to use your identity or use your account. You can not assign or
    transfer your account to any other party. You must maintain the security
    and confidentiality of your account password and any identification we
    provide to you. In the event of any disclosure of your password, in any
    manner whatsoever resulting in any unauthorized or unauthorized use of your
    account or identity, transactions received from such unauthorized or
    unauthorized use shall still be considered valid transactions, except You
    notify us of this matter before any transaction outside of your authority.
</p>
<p>
    5. You can only have one POPBOX account with one mobile phone number
</p>
<p>
    6. You promise that you will use the Application only for the intended
    purpose to use the POPBOX Service. You are not allowed to abuse or use the
    Application for fraudulent purposes or cause inconvenience to others or
    make fake orders.
</p>
<p>
    7. You may not harm, alter or modify the Application in any way. We are not
    responsible if you do not have the appropriate device or if you have
    downloaded the wrong version of Apps for your device. We reserve the right
    to prohibit you from using the Application further if you use the
    Application with an unauthorized device or for any purpose other than the
    intended purpose for use of the Application. You promise that you will only
    use this application as it should.
</p>
<p>
    8. You understand and agree that your use of the Application will also be
    subject to our Privacy Policy as may be amended from time to time. By using
    the Application, you are also granting approval as required under our
    Privacy Policy.
</p>
<p>
    9. Applications should not be used to deliver goods in the form of:
</p>
<p>
    1. Drugs and Substances, Animals, Firearms
</p>
<p>
    2. Flammable Goods / Explosive
</p>
<p>
    3. Wet Food, Fresh Food, Liquor
</p>
<p>
    4. Items that exceed POPBOX size locker specifications:
</p>
<table>
    <tbody>
        <tr>
            <td width="79" valign="center">
                <p align="center">
                    Type
                </p>
            </td>
            <td width="79" valign="center">
                <p align="center">
                    Size
                </p>
            </td>
        </tr>
        <tr>
            <td width="79" valign="center">
                <p align="center">
                    Document
                </p>
            </td>
            <td width="79" valign="center">
                <p align="center">
                    1 KG
                </p>
            </td>
        </tr>
        <tr>
            <td width="79" valign="center">
                <p align="center">
                    Small
                </p>
            </td>
            <td width="79" valign="center">
                <p align="center">
                    1 – 3 KG
                </p>
            </td>
        </tr>
        <tr>
            <td width="79" valign="center">
                <p align="center">
                    Medium
                </p>
            </td>
            <td width="79" valign="center">
                <p align="center">
                    3 – 6 KG
                </p>
            </td>
        </tr>
        <tr>
            <td width="79" valign="center">
                <p align="center">
                    Large
                </p>
            </td>
            <td width="79" valign="center">
                <p align="center">
                    6 – 10 KG
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    10. You must provide us with accurate and complete information about the
    type, size, photos of the items to be delivered using the application /
    inserted into the POPBOX locker.
</p>
<p>
    11. We reserve the right not to ship your items if we have reasonable
    grounds to suspect the goods you are shipping are goods that violate these
    terms of use or applicable laws and regulations.
</p>
<p>
    12. We or the Service Provider do not guarantee the availability of the
    chosen locker size is still available when the user arrives at the
    location.
</p>
<p>
    <strong>3. Payment</strong>
    <strong></strong>
</p>
<p>
    1. Download and / or use of this Application is free of cost. However, the
    internet connection required to use the Service, and any related expenses
    (such as mobile data charges) incurred by the use of the Service are your
    sole responsibility and solely liable to you.
</p>
<p>
    2. Rates applicable to the Service may be found on the Application. We may
    change or update rates from time to time.
</p>
<p>
    3. Payment of service available in this application can be done by using
    POPSEND Balance, POPSEND Balance can be obtained by Top Up in this
    application.
</p>
<p>
    4. POPSEND balance is not a savings account.
</p>
<p>
    5. POPBOX may, from time to time, increase the amount of your POPSEND
    balance in your account as part of its promotion (for example, referral
    code).
</p>
<p>
    <strong>4. Terms and Conditions</strong>
    <strong></strong>
</p>
<p>
    1. Customers are required to provide correct and complete information about
    the type and specification of the goods to be shipped.
</p>
<p>
    2. POPBOX does not provide special box for delivery. The customer is
    responsible for properly packing the items to be shipped. For fragile items
    made of glass, ceramics and fresh flowers, it is recommended to be
    specially packaged. POPBOX shall not be liable for any damage or alteration
    of any kind incurred on the delivery of such goods.
</p>
<p>
    3. POPBOX does not provide delivery service for the goods which are
    included below:
</p>
<p>
    1. Items prohibited by the authorities to be owned or circulated.
</p>
<p>
    2. Pets.
</p>
<p>
    3. Prohibited Goods, which are already mentioned in points of Conditions
    for Using the Application.
</p>
<p>
    4. Each voucher code can only be used once per account for each promotion.
</p>
<p>
    These Terms and Conditions of Users may change from time to time.
</p>
<p>
    Matters that have not been regulated or sufficiently regulated in these
    Terms and Conditions, the settlement of arrangements shall be decided by
    and on the mutual consent of the Parties.
</p>