<h2><strong>Syarat dan Ketentuan Pengguna Aplikasi PopBox</strong></h2>
<p><strong>Definisi</strong></p>
<p>PopBox adalah aplikasi layanan logistik dari/ke loker atau alamat dimana pelanggan dapat melakukan proses order pengiriman via aplikasi, memasukkan barang via loker PopBox atau melakukan permintaan jemput barang dari lokasi alamat dan barang akan dikirimkan ke loker lainnya atau alamat penerima. PopBox juga memiliki fitur titip barang &lsquo;PopSafe&rsquo; dimana pelanggan dapat menggunakan loker untuk menitip barang sementara. Selain itu PopBox juga memiliki fitur &lsquo;On Demand&rsquo; dimana pelanggan dapat melakukan pengiriman untuk laundri pakaian/sepatu atau lainnya yang kemudian dikirimkan ke vendor untuk dibersihkan dan akan dikirimkan kembali ke loker setelah selesai.</p>
<p>Dengan mengunduh, memasang dan/atau menggunakan aplikasi POPBOX (Popsend), Anda setuju bahwa Anda telah membaca, memahami dan menerima dan menyetujui Syarat dan Ketentuan Penggunaan ini ("Syarat dan Ketentuan Penggunaan"). Ketentuan Penggunaan ini merupakan suatu perjanjian sah antara Anda dan PT Popbox Asia Services dan Layanan dan Aplikasi (sebagaimana didefinisikan di bawah ini).</p>
<p>Silahkan membatalkan akun Anda (jika Anda telah mendaftar untuk Aplikasi) dan secara permanen menghapus aplikasi dari perangkat Anda jika Anda tidak setuju atau tidak ingin masuk ke dalam Ketentuan Penggunaan.</p>
<p>MOHON ANDA MEMERIKSA KETENTUAN PENGGUNAAN DAN KEBIJAKAN PRIVASI KAMI DENGAN SEKSAMA SEBELUM MENGUNDUH APLIKASI ATAU MENGGUNAKAN LAYANAN KAMI UNTUK PERTAMA KALI.</p>
<p><strong>1. Hal-hal Umum</strong></p>
<p>1. PT. PopBox Asia Services adalah suatu perseroan yang didirikan berdasarkan hukum Negara Republik Indonesia.</p>
<p>2. Aplikasi ini merupakan aplikasi perangkat lunak yang berfungsi sebagai sarana untuk melakukan pengiriman parcel menggunakan Smart Locker Popbox yang tersedia di berbagai tempat strategis. Jenis layanan yang dapat digunakan melalui aplikasi adalah :</p>
<ul>
<li>Pengiriman Barang</li>
<li>Titip Barang.</li>
<li>Melacak &amp; Riwayat pengiriman Barang</li>
<li>Isi Ulang saldo / Top Up Saldo</li>
<li>Sevice On Demand</li>
<li>Layanan lain yang dapat kami tambahkan dari waktu ke waktu.</li>
</ul>
<p><strong>2. Ketentuan Untuk Menggunakan Aplikasi</strong></p>
<p>1. Anda menyatakan dan menjamin bahwa Anda adalah individu yang secara hukum berhak untuk mengadakan perjanjian yang mengikat berdasarkan hukum Negara Republik Indonesia, khususnya Ketentuan Penggunaan, untuk menggunakan Aplikasi dan bahwa Anda telah berusia minimal 17 tahun atau sudah menikah. Jika tidak, kami atau Penyedia Layanan terkait, berhak berdasarkan hukum untuk membatalkan perjanjian yang dibuat dengan Anda. Anda selanjutnya menyatakan dan menjamin bahwa Anda memiliki hak, wewenang dan kapasitas untuk menggunakan Layanan dan mematuhi Ketentuan Penggunaan Layanan dan Aplikasi.</p>
<p>2. Kami mengumpulkan dan memproses informasi pribadi Anda, seperti nama, alamat surat elektronik (surel / e-mail), dan nomor telepon seluler Anda ketika Anda mendaftar. Anda harus memberikan informasi yang akurat dan lengkap, memperbaharui informasi dan setuju untuk memberikan kepada kami bukti identitas apapun yang secara wajar dapat kami mintakan. Jika informasi pribadi yang Anda berikan kepada kami ada yang berubah, misalnya, jika Anda mengubah alamat surel, nomor telepon, atau jika Anda ingin membatalkan akun Anda, mohon perbaharui rincian informasi Anda dengan mengirimkan permintaan Anda kepada kami. Kami akan memberlakukan perubahan yang diminta.</p>
<p>3. Anda hanya dapat menggunakan Aplikasi ketika Anda telah mendaftar pada Aplikasi tersebut. Setelah Anda berhasil mendaftarkan diri, Aplikasi akan memberikan Anda suatu akun pribadi yang dapat diakses dengan kata sandi yang Anda pilih.</p>
<p>4. Hanya Anda yang dapat menggunakan akun Anda sendiri dan Anda berjanji untuk tidak memberikan wewenang kepada orang lain untuk menggunakan identitas Anda atau menggunakan akun Anda. Anda tidak dapat menyerahkan atau mengalihkan akun Anda kepada pihak lain. Anda harus menjaga keamanan dan kerahasiaan kata sandi akun Anda dan setiap identifikasi yang kami berikan kepada Anda. Dalam hal terjadi pengungkapan atas kata sandi Anda, dengan cara apapun, yang mengakibatkan setiap penggunaan yang tidak sah atau tanpa kewenangan atas akun atau identitas Anda, transaksi yang diterima dari penggunaan yang tidak sah atau tanpa kewenangan tersebut masih akan dianggap sebagai transaksi yang sah, kecuali Anda memberitahu kami tentang mengenai hal tersebut sebelum terjadi transaksi diluar kewenangan anda.</p>
<p>5. Anda hanya dapat memiliki satu akun POPBOX dengan satu nomor handphone</p>
<p>6. Anda berjanji bahwa Anda akan menggunakan Aplikasi hanya untuk tujuan yang dimaksud untuk menggunakan Layanan POPBOX. Anda tidak diperbolehkan untuk menyalahgunakan atau menggunakan Aplikasi untuk tujuan penipuan atau menyebabkan ketidaknyamanan kepada orang lain atau melakukan order palsu.</p>
<p>7. Anda tidak diperkenankan untuk membahayakan, mengubah atau memodifikasi Aplikasi dengan cara apapun. Kami tidak bertanggung jawab jika Anda tidak memiliki perangkat yang sesuai atau jika Anda telah mengunduh versi Aplikasi yang salah untuk perangkat Anda. Kami berhak untuk melarang Anda untuk menggunakan Aplikasi lebih lanjut jika Anda menggunakan Aplikasi dengan perangkat yang tidak sah atau untuk tujuan lain selain daripada tujuan yang dimaksud untuk penggunaan Aplikasi ini. Anda berjanji bahwa Anda hanya akan menggunakan aplikasi ini sebgaimana mestinya.</p>
<p>8. Anda memahami dan setuju bahwa penggunaan Aplikasi oleh Anda akan tunduk pula pada Kebijakan Privasi kami sebagaimana dapat diubah dari waktu ke waktu. Dengan menggunakan Aplikasi, Anda juga memberikan persetujuan sebagaimana dipersyaratkan berdasarkan Kebijakan Privasi kami.</p>
<p>9. Aplikasi tidak boleh digunakan untuk melakukan pengiriman barang berupa :</p>
<ul>
<li>uang &amp; perhiasan (dalam bentuk kertas maupun benda berharga lainnya)</li>
<li>narkotika dan segala substansi psikoatif lainnya</li>
<li>benda berharga (barang antik, akta, surat tanah, ijazah, dll)</li>
<li>binatang, tanaman atau lainnya</li>
<li>benda-benda pornografi</li>
<li>barang yang segar (buah, sayur, daging, lainnya)</li>
<li>barang pecah belah</li>
<li>senjata, amunisi, benda lainnya yang mudah terbakar/meledak</li>
<li>barang yang basah, bocor, menimbulkan bau dan lainnya</li>
<li>barang yang dapat menyebabkan resiko kesehatan, kerusakan dan lainnya</li>
<li>barang lainnya yang dilarang sesuai oleh peraturan Undang-Undang Republik Indonesia</li>
<li>barang yang memerlukan ijin khusus dalam pengiriman</li>
<li>Barang yang melebihi spesifikasi ukuran locker PopBox :</li>
</ul>
<table>
<tbody>
<tr>
<td valign="center" width="74">
<p align="center">Jenis</p>
</td>
<td valign="center" width="84">
<p align="center">Ukuran</p>
</td>
</tr>
<tr>
<td valign="center" width="74">
<p align="center">Document</p>
</td>
<td valign="center" width="84">
<p align="center">1 KG</p>
</td>
</tr>
<tr>
<td valign="center" width="74">
<p align="center">Small</p>
</td>
<td valign="center" width="84">
<p align="center">1 &ndash; 3 KG</p>
</td>
</tr>
<tr>
<td valign="center" width="74">
<p align="center">Medium</p>
</td>
<td valign="center" width="84">
<p align="center">3 &ndash; 6 KG</p>
</td>
</tr>
<tr>
<td valign="center" width="74">
<p align="center">Large</p>
</td>
<td valign="center" width="84">
<p align="center">6 &ndash; 10 KG</p>
</td>
</tr>
</tbody>
</table>
<p>10. Anda harus memberikan kepada kami informasi yang akurat dan lengkap mengenai jenis, ukuran, Foto barang yang akan dikirimkan menggunakan aplikasi/ dimasukan ke locker POPBOX.</p>
<p>11. Kami berhak untuk tidak mengirimkan barang anda jika kami memiliki alasan yang wajar untuk mencurigai barang yang anda kirimkan adalah barang yang melaggar ketentuan penggunaan ini atau hukum dan peraturan undang &ndash; undang yang berlaku.</p>
<p>12. Kami atau Penyedia Layanan tidak menjamin ketersediaan ukuran locker yang sudah dipilih masih tersedia saat pengguna sampai dilokasi.</p>
<p><span style="font-weight: 400;">13. Pengguna aplikasi harus bertanggung jawab atas penggunaan yang tidak sesuai dengan tata tertib dan penggunaan aplikasi yang mengakibatkan segala kerugian baik materiil maupun imateriil dan melakukan penggantian rugi sesuai dengan kerugian yang ditimbulkan dan membebaskan pihak PopBox atas segala tuntutan yang muncul akibatnya</span></p>
<p><strong>3. Pembayaran</strong></p>
<p>1. Pengunduhan dan/atau penggunaan Aplikasi ini adalah bebas biaya. Namun demikian, koneksi internet yang dibutuhkan untuk menggunakan Layanan, dan setiap biaya terkait (misalnya biaya data ponsel) yang ditimbulkan oleh penggunaan Layanan tersebut adalah tanggung jawab eksklusif anda dan semata-mata dibebankan kepada anda.</p>
<p>2. Tarif yang berlaku untuk Layanan dapat ditemukan pada Aplikasi. Kami dapat mengubah atau memperbaharui tarif dari waktu ke waktu.</p>
<p>3. Pembayaran layanan yang ada di aplikasi ini dapat dilakukan dengan menggunakan Saldo POPSEND, Saldo POPSEND dapat diperoleh dengan cara melakukan Isi Saldo / Top Up melalui aplikasi ini.</p>
<p>4. Saldo POPSEND bukan merupakan tabungan dan karena itu tidak termasuk dalam pengaturan oleh Lembaga Penjamin Simpanan Indonesia (LPS).</p>
<p>5. POPBOX dapat, dari waktu ke waktu, menambah jumlah Saldo POPSEND anda di akun anda sebagai bagian dari promosinya (misalnya, kode rujukan / referral code).</p>
<p><strong>4. Syarat dan Ketentuan</strong></p>
<p>1. Pelanggan wajib memberikan informasi yang benar dan lengkap mengenai jenis dan spesifikasi barang yang akan dikirimkan.</p>
<p>2. POPBOX tidak menyediakan box khusus untuk pengiriman. Pelanggan bertanggung jawab untuk mengemas dengan layak barang yang akan dikirimkan. Untuk barang rapuh yang terbuat dari kaca, keramik dan bunga segar, disarankan agar dikemas secara khusus. POPBOX tidak bertanggung jawab untuk kerusakan atau perubahan bentuk yang terjadi atas pengiriman barang-barang tersebut.</p>
<p>3. POPBOX tidak memberikan layanan pengiriman untuk barang yang termasuk dibawah ini:</p>
<ul>
<li>Barang yang dilarang oleh pihak berwajib untuk dimiliki atau diedarkan.</li>
<li>Binatang peliharaan.</li>
<li>Barang - barang yang dilarang, yang sudah disebutkan pada poin Ketentuan Untuk Menggunakan Aplikasi.</li>
<li>Setiap kode voucher hanya bisa digunakan satu kali per akun untuk setiap promosi.</li>
</ul>
<p align="justify">Syarat Dan Ketentuan Pengguna ini dapat berubah sewaktu-waktu.</p>
<p align="justify">Hal-hal yang belum diatur atau belum cukup diatur dalam Syarat dan Ketentuan ini, penyelesaian pengaturannya akan diputuskan oleh dan atas permufakatan bersama dari Para Pihak.</p>
<p align="justify"><strong>5. Hubungi kami</strong></p>
<p>Syarat dan ketentuan dapat berubah sewaktu-waktu tanpa pemberitahuan. Untuk informasi lebih lanjut dapat hubungi kami di 021- 2122538719 atau info@popbox.asia (Senin - Jumat: 08.00 - 17.00 dan Sabtu 08.00 - 14.00) atau melalui social media PopBox Asia</p>