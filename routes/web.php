<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/', 'LandingController@index');
Route::get('/', function(Request $request, $code = null) {
    // $ip= \Request::ip();
    // $ip = \Request::getClientIp(true);
    // dd($code);
    if($code != null) {
        if( strtolower($code) == "id" ) {
            App::setLocale("id");
        } else {
            App::setLocale("en");
        }
    } else {
    $ip = request()->ip();
    $position = Location::get()->countryCode;
    // $position = Location::get($ip);
    // dd($position);
    if( strtolower($position) == "id" ) {
    	App::setLocale("id");
    } else {
    	App::setLocale("en");
    }
    }
    return view('layouts.newmain');
});

Route::get('/{code}', function ($code) {
    if( strtolower($code) == "id" ) {
    	App::setLocale("id");
    } else {
    	App::setLocale("en");
    }
    return view('layouts.newmain');
});

Route::get('/setLangToID', function() {
    App::setLocale("id");
    return redirect('layouts/newmain');
});

Route::get('/setLangToEN', function() {
    App::setLocale("en");
    return redirect('layouts/newmain');
});

Route::get('/contact', 'LandingController@contact');
Route::get('/apps', 'LandingController@apps');
Route::group(['prefix'=>'mobile'],function(){
    Route::get('faq',function(Request $request){
        $ip = $request->ip();
        $countryCode = 'ID';
        if (!empty($ip)){
            $result = @file_get_contents("http://www.geoplugin.net/php.gp?ip=$ip");
            if (!empty($ip)){
                $result = unserialize($result);
                if (isset($result['geoplugin_countryCode'])){
                    if (!empty($result['geoplugin_countryCode'])) {
                        $countryCode = $result['geoplugin_countryCode'];
                    }
                }
            }
        }
        $countryCode = strtoupper($countryCode);

        if ($countryCode == 'MY') {
            return view('webview.faq-my');
        } else {
            return view('webview.faq');
        }
    });
    Route::get('faq/my',function(Request $request){return view('webview.faq-my');});
    Route::get('tnc/register',function(){ return view('webview.tnc-register');});
    Route::get('tnc/register/my',function(){ return view('webview.tnc-register-eng');});
    Route::get('tnc/order',function(){ return view('webview.tnc-order-id');});
    Route::get('tnc/order/eng',function(){ return view('webview.tnc-order-eng');});
    Route::get('tnc/order/my',function(){ return view('webview.tnc-order-my');});
});
